﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
namespace St0rm.TF2
{
    using System;
    using System.Threading;
    using Core;
    using Core.Plugin;
    using SSQWrapper;
    using Timer = System.Timers.Timer;

    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                Console.Title = "St0rmServ - TF2 Edition";

                try
                {
                    SSQ ssq = SSQ.Instance;
                    try
                    {
                        ssq.SetGameServer(Config.TF2Host, Config.TF2Port);
                    }
                    catch (RequestFailedException)
                    {
                        Console.WriteLine(
                                          "Could not get challenge number, please ensure the server is online and the IP/Port are correctly set. Press enter to exit...");

                        Console.ReadKey();
                        Environment.Exit(1);
                    }
                    ssq.RCONPassword = Config.RconPass;
                    ssq.EnableLogs(Config.LogHost, Config.LogPort);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.GetBaseException());
                    Console.ReadKey();
                    Environment.Exit(1);
                }

                Console.WriteLine("Spawning threads...");
                ThreadPool.SetMinThreads(20, 10);
                ThreadPool.SetMaxThreads(100, 50);

                IRC irc = IRC.Instance;

                Console.WriteLine("Refreshing player list...");
                Player.RefreshList();
                Console.WriteLine("Started bot for " + SSQ.Instance.GetInfoReply().hostname);

                TF2EventHandler.InitializeRconParsers();
                SSQ.Instance.HideNextRcon();
                Remote.Raw("status");

                TF2EventHandler.InitializeEvents();
                TF2EventHandler.InitializeIrcCommands();

                Console.WriteLine("Loading plugins...");
                PluginLoader.Instance.LoadAll();

                var announce_bot = new Timer(247983);
                announce_bot.Elapsed += (sender, t_args) =>
                                        {
                                            if (Player.GetPlayerList().Length > 0)
                                                Remote.BotMessage(
                                                                  "This server is being monitored by St0rmServ IRC-based regulation bot, www.st0rm.net!");
                                        };
                announce_bot.Start();

                //Re-challenge just in case
                var challenge_check = new Timer(3600000);
                challenge_check.Elapsed +=
                    (sender, t_args) => SSQ.Instance.SetGameServer(Config.TF2Host, Config.TF2Port);
                challenge_check.Start();
                var hearbeat = new Timer(300000);
                hearbeat.Elapsed += (sender, t_args) =>
                                    {
                                        if (
                                            new TimeSpan((DateTime.Now.Ticks) - SSQ.Instance.LastRconReply.Ticks).
                                                TotalSeconds > 300)
                                        {
                                            try
                                            {
                                                SSQ.Instance.SetGameServer(Config.TF2Host, Config.TF2Port);
                                            }
                                            catch (RequestFailedException)
                                            {
                                                Console.WriteLine(
                                                                  "No rcon challenge response from the server, retrying in five minutes...");
                                                IRC.Instance.Broadcast(
                                                                       "No rcon challenge response from the server, retrying in five minutes...");
                                            }
                                        }
                                    };
                hearbeat.Start();

                //Just in case there is a discrephency with our list and the servers
                //we'll check every five minutes

                // ReSharper disable InconsistentNaming
                var sync_player_list      = new Timer(300000);
                sync_player_list.Elapsed += (sender, t_args) => Player.RefreshList();
                sync_player_list.Start();
                // ReSharper enable InconsistentNaming

                // PluginLoader can only be accessed from the exe so we have to add the command here unfortunately
                IRC.Instance.OnIrcCommand += (command, x, y) =>
                                             {
                                                 if (command.ToLower().Equals("plugins"))
                                                 {
                                                     IRC.Client.Send("NOTICE {0} :{1}", y.User.Name, "Loaded plugins: ");
                                                     foreach (var plugin in PluginLoader.Instance.GetPlugins())
                                                     {
                                                         IRC.Client.Send("NOTICE {0} :{1}",
                                                             y.User.Name,
                                                             String.Format("{0} version {1} by {2}",
                                                                 plugin.Name,
                                                                 plugin.Version,
                                                                 plugin.Author));
                                                     }
                                                 }
                                                 else if (command.ToLower().Equals("version"))
                                                 {
                                                     IRC.Client.SendM(y.Channel.Name,
                                                         "St0rmServ TF2 Edition, www.st0rm.net by Ben Buzbee ben@st0rm.net");
                                                 }

                                                 return false;
                                             };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
