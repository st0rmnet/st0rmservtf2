﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
namespace St0rm.Trivia {
    class RankManager {
        public static RankManager Instance = new RankManager();

        private RankManager() {
        }

        public long GetRank(string nick, string channel) {
            MySqlCommand command = Database.Connection.CreateCommand();
            command.CommandText = @"SELECT count(*)+1 AS rank FROM `scores` WHERE `channel`=@chan AND `score` > (SELECT `score` FROM `scores` WHERE `nick`=@nick AND `channel`=@chan)";
            command.Prepare();
            command.Parameters.AddWithValue("@nick", nick);
            command.Parameters.AddWithValue("@chan", channel);
            MySqlDataReader reader = command.ExecuteReader();
            Int64 rank;
            if (reader.Read())
                rank = reader.GetInt64(0);
            else
                rank = 0;
            reader.Close();
            return rank;
        }
        public long GetScore(string nick, string channel) {
            MySqlCommand command = Database.Connection.CreateCommand();
            command.CommandText = @"SELECT `score` FROM `scores` WHERE `channel`=@chan AND `nick`=@nick";
            command.Prepare();
            command.Parameters.AddWithValue("@chan",channel);
            command.Parameters.AddWithValue("@nick", nick);
            MySqlDataReader reader = command.ExecuteReader();
            Int64 score;
            if (reader.Read())
                score = reader.GetInt64(0);
            else
                score = 0;
            reader.Close();
            return score;
        }
        public string GetNickFromRank(int rank, string channel) {
            MySqlCommand command = Database.Connection.CreateCommand();
            command.CommandText = @"SELECT `nick` FROM `scores` WHERE `channel`=@chan ORDER BY score DESC LIMIT @rank";
            command.Prepare();
            command.Parameters.AddWithValue("@chan", channel);
            command.Parameters.AddWithValue("@rank", rank);
            MySqlDataReader reader = command.ExecuteReader();
            string last_name = null;
            while (reader.Read()) {
                last_name = reader.GetString(0);
            }
            reader.Close();
            return last_name;
        }
    }
}
