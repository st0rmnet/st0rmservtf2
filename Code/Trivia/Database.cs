﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace St0rm.Trivia {
    class Database {
        private static Database instance = new Database();

        private MySqlConnection connection = null;

        public static MySqlConnection Connection {
            get {
                if (instance.connection == null || instance.connection.State != System.Data.ConnectionState.Open) {
                    instance.connection = new MySqlConnection();
                    
                    instance.connection.ConnectionString = String.Format("Server={0};Database={1};Uid={2};Pwd={3};", 
                        Config.DBHost, Config.DBName, Config.DBUser, Config.DBPass);
                    instance.connection.Open();
                }
                return instance.connection;
            }
        }
    }
}
