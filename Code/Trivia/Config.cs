﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace St0rm.Trivia {
    class Config {
        const string CONFIG_FILE = "trivia.conf";
        public static Config Instance = new Config();
        string nick = "St0rmTriviaTest", user = "Trivia", server = "irc.St0rm.net";
        string dbhost = "www.st0rm.net", dbuser = "trivia2", dbpass = "maow", dbname = "trivia";
        ushort port = 6667;
        string[] channels = { "" };

        public static ushort IRCPort {
            get { return Instance.port; }
            set { Instance.port = value; }
        }
        public static string IRCServer {
            get { return Instance.server; }
            set { Instance.server = value; }
        }
        public static string Nick {
            get { return Instance.nick; }
            set { Instance.nick = value; }
        }
        public static string User {
            get { return Instance.user; }
            set { Instance.user = value; }
        }
        public static string[] Channels {
            get { return Instance.channels; }
        }
        public static string DBName {
            get { return Instance.dbname; }
        }
        public static string DBUser {
            get { return Instance.dbuser; }
        }
        public static string DBHost {
            get { return Instance.dbhost; }
        }
        public static string DBPass {
            get { return Instance.dbpass; }
        }

        static Config() {
            Load();
        }
        public static void Load() {
            if (!File.Exists(CONFIG_FILE)) {
                Console.WriteLine("No configuration file...");
                Environment.Exit(1);
            }
            FileStream f_is = File.OpenRead(CONFIG_FILE);
            TextReader reader = new StreamReader(f_is);
            string line;
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex( 
                @"^(?!\s*;)\s*([^=]+)=(.*)$"    
            );
            System.Text.RegularExpressions.Match match;
            while ((line = reader.ReadLine()) != null) {
                match = regex.Match(line);
                if (match.Success) {
                    GroupCollection groups = match.Groups;
                    System.Collections.IEnumerator enumerator = groups.GetEnumerator();
                    enumerator.MoveNext();
                    enumerator.MoveNext();
                    string item = ((Group)enumerator.Current).Value;
                    enumerator.MoveNext();
                    string value = ((Group)enumerator.Current).Value;
                    SetItem(item, value);
                }
                else 
                    continue;
            }
            reader.Close();
            
        }
        public static void SetItem(string item, string value) {
            item = item.Trim();
            value = value.Trim();
            try {
                if (String.Compare(item, "IRCPort", true) == 0)
                    Instance.port = ushort.Parse(value);
                else if (String.Compare(item, "IRCServer", true) == 0)
                    Instance.server = value;
                else if (String.Compare(item, "Nick", true) == 0)
                    Instance.nick = value;
                else if (String.Compare(item, "User", true) == 0)
                    Instance.user = value;
                else if (String.Compare(item, "DBName", true) == 0)
                    Instance.dbname = value;
                else if (String.Compare(item, "DBUser", true) == 0)
                    Instance.dbuser = value;
                else if (String.Compare(item, "DBHost", true) == 0)
                    Instance.dbhost = value;
                else if (String.Compare(item, "DBPass", true) == 0)
                    Instance.dbpass = value;
                else if (String.Compare(item, "Channels", true) == 0)
                    Instance.channels = value.Split(',');
                
            }
            catch (FormatException) {
                Console.WriteLine("Configuration error, please check your configuration...");
                Environment.Exit(1);
            }


        }
    }
}
