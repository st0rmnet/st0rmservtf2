﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace St0rm.Trivia {
    class Question {
        /// <summary>
        /// The next five questions. Cached for faster questioning
        /// </summary>
        /// <remarks>
        /// Lock this object! the Fill() method will usually be called from another thread
        /// </remarks>
        private const ushort QUEUE_SIZE = 50;
        private static Queue<Question> questionQueue = new Queue<Question>(QUEUE_SIZE);

        private string _questions;
        private string[] _answers;
        
        public string Questions { get { return _questions; } }
        public string[] Answers { get { return _answers; } }
        public bool IsYear {
            get {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^\\d{4}$");
                return regex.IsMatch(Answers[0]);
            } 
        }

        
        public Question(string questions, string[] answers) {
            _questions = questions.Trim();
            for (byte i = 0; i < answers.GetLength(0); i++)
                answers[i] = answers[i].Trim();
            _answers = answers;
        }
        static Question() {
            FillNext();
        }
        public static Question GetRandomQuestion() {
            lock (questionQueue) {
                if (questionQueue.Count > 0) {
                    Question q = questionQueue.Dequeue();
                    if (questionQueue.Count < (QUEUE_SIZE/2.0f))
                        new System.Threading.Thread(new System.Threading.ThreadStart(FillNext)).Start();
                    return q;
                }
                else {
                    FillNext();
                    return GetRandomQuestion();
                }
            }
        }
        public override string ToString() {
            return Questions;
        }
        /// <summary>
        /// Gets this questions hints, revealing the specified percentage of the first answer
        /// </summary>
        /// <param name="percentage">Percentage of answer to reveal. 1 would be all</param>
        /// <returns>The hint for the question at given percent</returns>
        public string GetHint(double percentage) {
            StringBuilder sb = new StringBuilder(Answers[0].Length);
            foreach (string word in Answers[0].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries)) {
                sb.Append(RevealString(word, percentage));
                sb.Append(' ');
            }
            return sb.ToString().Trim();
        }
        /// <summary>
        /// Fills the queue that contains the next questions
        /// </summary>
        public static void FillNext() {
            lock (questionQueue) {
                if (questionQueue.Count < QUEUE_SIZE) {
                    MySqlCommand cmd = Database.Connection.CreateCommand();
                    cmd.CommandText = "SELECT * FROM questions ORDER BY rand() LIMIT @limit";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@limit", QUEUE_SIZE - questionQueue.Count);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read()) {
                        questionQueue.Enqueue(new Question(reader.GetString(0),reader.GetString(1).Split('*')));
                    }
                    reader.Close();
                }
            }
        }
        /// <summary>
        /// Reveals the given percentage of the word and hides the rest.
        /// </summary>
        /// <param name="word">Word to reveal</param>
        /// <param name="percentage">Percentage of word to reveal</param>
        /// <returns>Hidden/Revealed string</returns>
        string RevealString(string word, double percentage) {
            StringBuilder sb = new StringBuilder(word.Length);
            int charsToShow = (int)Math.Ceiling(percentage * word.Length);
            int charsToHide = word.Length - charsToShow;
            if (charsToShow > 0)
                sb.Append(word.Substring(0, charsToShow));
            for (int i = 0; i < charsToHide; i++)
                sb.Append('-');

            return sb.ToString();
        }
    }
}
