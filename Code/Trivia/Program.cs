﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MySql.Data.MySqlClient;
namespace St0rm.Trivia {
    class Program {
        static void Main(string[] args) {
            Console.Title = "Trivia Bot";

            IRC.Client.Connect(Config.IRCServer, Config.IRCPort);
            IRC.Client.Login(Config.Nick, Config.User);
            IRC.Client.RfcJoin(Config.Channels);
            Thread thread = new Thread(IRC.Client.Listen);
            thread.Start();   
            
        }
    }
}
    