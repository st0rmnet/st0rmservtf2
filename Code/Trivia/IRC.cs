﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using Meebey.SmartIrc4net;
using System.Threading;

namespace St0rm.Trivia {
    class IRC {
        static IrcClient clientInstance;
                public static IrcClient Client {
            get {
                if (clientInstance == null) Initialize();
                return clientInstance;
            }
        }

        static void Initialize() {
            clientInstance = new IrcClient();
            Client.OnJoin += Join;
            Client.OnChannelMessage += Message;
            Client.ActiveChannelSyncing = true;
        }
        
       static void Join(object sender, JoinEventArgs args) {
            foreach (string c in Config.Channels) {
                if (string.Compare(c, args.Channel, true) == 0)
                    return;
            }
            Client.RfcPart(args.Channel,"Not in my list.");
        }
       static void Message(object sender, IrcEventArgs args) {
           IrcMessageData data = args.Data;
           if (data.Type == ReceiveType.ChannelMessage && data.Message.ToLower().StartsWith("!trivia ")) {
               string[] parameters = new string[data.MessageArray.GetLength(0) - 1];
               Array.Copy(data.MessageArray, 1, parameters, 0, parameters.GetLength(0));
               if (parameters[0].ToLower().Equals("start")) {
                   Game game = Game.GetGame(data.Channel);
                   if (game != null) {
                       if (game.IsStopped)
                           game.Resume();
                       else
                           Client.RfcNotice(data.Nick, "Trivia already started. Use \"stop\" to stop trivia.");

                       return;
                   }
                   else {
                       game = Game.CreateGame(data.Channel);
                       Thread t;
                       game.StartGame(out t);
                   }
               }
               else if (parameters[0].ToLower().Equals("stop")) {
                   Game g;
                   if ((g = Game.GetGame(data.Channel)) == null)
                       Client.RfcNotice(data.Nick, "Start trivia by using the \"start\" command");
                   else if (!g.IsStopped) {
                       g.StopGame();
                   }
               }
               else if (parameters[0].ToLower().Equals("next")) {
                    Game g;
                    if ((g = Game.GetGame(data.Channel)) == null)
                        Client.RfcNotice(data.Nick, "Start trivia by using the \"start\" command");
                    else if (g.IsStopped)
                        Client.RfcNotice(data.Nick, "Trivia is ending after this question, let it play through.");
                    else {
                        ChannelUser user = Client.GetChannelUser(data.Channel, data.Nick);
                        if (user.IsOp || g.HintNumber >= 2)
                            g.Next();
                        else
                            Client.RfcNotice(data.Nick, "You may skip this question only after the second hint.");
                    }
                   
               }
               else if (parameters[0].ToLower().Equals("score")) {
                   string user;
                   int rank;
                   if (((parameters.GetLength(0) == 1) && ((user = data.Nick) != null)) || ((!Regex.IsMatch(parameters[1],"^\\d+$") && ((user = parameters[1])!= null)))) {
                       long uRank = RankManager.Instance.GetRank(user, data.Channel);
                       long uScore = RankManager.Instance.GetScore(user, data.Channel);
                       if (uRank == 0 || uScore == 0)
                           Client.RfcPrivmsg(data.Channel, "No rank found for user.");
                       else
                           Client.RfcPrivmsg(data.Channel, String.Format("{0} is rank {1} with a score of {2}",user,uRank,uScore));

                   }
                   else if (parameters.GetLength(0) > 1 && Regex.IsMatch(parameters[1], @"^\d+$")) {
                       rank = int.Parse(parameters[1]);
                       string uUser = RankManager.Instance.GetNickFromRank(rank,data.Channel);
                       if (uUser == null)
                           Client.RfcPrivmsg(data.Channel, "No rank found for user.");
                       long uScore = RankManager.Instance.GetScore(uUser,data.Channel);
                       if (rank == 0 || uScore == 0)
                           Client.RfcPrivmsg(data.Channel, "No rank found for user.");
                       else
                           Client.RfcPrivmsg(data.Channel, String.Format("{0} is rank {1} with a score of {2}", uUser, rank, uScore));
                   }
               }
           }
           else if (data.Type == ReceiveType.ChannelMessage) {
               Game game = Game.GetGame(data.Channel);
               if (game == null)
                   return;

               game.Guess(data.Nick, data.Message);
           }
        }
    }
}
