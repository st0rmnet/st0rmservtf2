﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meebey.SmartIrc4net;
using System.Threading;
using System.Text.RegularExpressions;
using System.Timers;
using MySql.Data.MySqlClient;
namespace St0rm.Trivia {
    class Game {
        private static LinkedList<Game> games = new LinkedList<Game>();

        private string channel;
        private Question[] previousQuestions = new Question[5];
        private Question currentQuestion = null;
        private Dictionary<string, ushort> scores = new Dictionary<string, ushort>(20);
        private bool stopped = false;
        private ushort questionNumber = 0;
        private bool answerable = false;
        private Thread thread = null;
        private System.Timers.Timer timer = new System.Timers.Timer();
        private byte hint = 0;
        private event OnStopHandler OnStop;
        private delegate void OnStopHandler();
        private Meebey.SmartIrc4net.NickChangeEventHandler nickChangeDelegate = null;


        public string Chan { get { return channel; } }
        public bool IsStopped { get { return stopped; } }
        public int HintNumber { get { return hint; } }

        private Game(string c) {
            channel = c;
            OnStop += new OnStopHandler(SaveScores);
        }


        /// <summary>
        /// Gets the current trivia game for the specified channel. Use createGame to make one. null if there is no game.
        /// </summary>
        /// <param name="c">The channel</param>
        /// <returns>Current trivia game. <b>null if there is no game.</b></returns>
        public static Game GetGame(string channel) {
            foreach (Game g in games) {
                if (channel.ToLower().Equals(g.Chan.ToLower()))
                    return g;
            }
  
            return null;
        }
        /// <summary>
        /// Adds a game for the specified channel. Returns the game. Can return null if a game already exists!
        /// </summary>
        /// <param name="c">The channel to create a game for.</param>
        /// <returns>The created game, or null if a game already exists. use getGame to get it.</returns>
        public static Game CreateGame(string channel) {
            if (GetGame(channel) == null) {
                Game g = new Game(channel);
                games.AddFirst(g);
                IRC.Client.OnNickChange += g.nickChangeDelegate = new Meebey.SmartIrc4net.NickChangeEventHandler(g.OnNickChange);
                return g;
            }
            return null;
        }
        /// <summary>
        /// Begins the trivia game on the specified thread.
        /// </summary>
        public void StartGame(out Thread t) {
            t = new Thread(Begin);
            thread = t;
            thread.Start(); 
        }
        /// <summary>
        /// Thread entry point
        /// </summary>
        void Begin() {
            try {
                Msg("A game of trivia has been started! The first question will be asked in five seconds...");
                Thread.Sleep(5000);
                Ask();
            }
            catch (ThreadAbortException) {
                Msg("Trivia has been stopped. Use \"trivia start\" to restart it.");
                games.Remove(this);
                OnStop();
            }
        }
        /// <summary>
        /// Asks a new question (Overrides currentQuestion)
        /// </summary>
        void Ask() {


            if (timer.Enabled)
                timer.Stop();
            if (stopped) {
                Msg("Stopping trivia. Good game!");
                games.Remove(this);
                IRC.Client.OnNickChange -= nickChangeDelegate;
                OnStop();
                return;
            }
            if (currentQuestion != null) {
                Array.Copy(previousQuestions, 0, previousQuestions, 1, 4); // Copy old previous questions over
                previousQuestions[0] = currentQuestion; // Set the last question to position 0
            }
            currentQuestion = Question.GetRandomQuestion();
            questionNumber++;
            if (currentQuestion.IsYear) {
                Msg("Modern year question! (#{0}) You have ten seconds to earn ten points: {1}", questionNumber, currentQuestion);
                Msg("Year hint: {0}", currentQuestion.GetHint(.5));
                timer = new System.Timers.Timer(10000);
                timer.AutoReset = true;
                timer.Elapsed += new ElapsedEventHandler(OutOfTime);
                timer.Start();
            }
            else {
                Msg("Question {0}: {1}", questionNumber, currentQuestion);
                hint = 1;
                timer = new System.Timers.Timer(12500);
                timer.Elapsed += new ElapsedEventHandler(GiveNextHint);
                timer.AutoReset = true;
                timer.Start();
            }
            answerable = true;
        }

        void GiveNextHint(object sender, ElapsedEventArgs args) {
            switch (hint) {
                case 1: 
                    GiveHint(.25);
                    if (timer.Enabled)
                        timer.Stop();
                    timer = new System.Timers.Timer(15000);
                    timer.Elapsed += new ElapsedEventHandler(GiveNextHint);
                    timer.AutoReset = true;
                    timer.Start();
                    break;
                case 2:
                    GiveHint(.5);
                    if (timer.Enabled)
                        timer.Stop();
                    timer = new System.Timers.Timer(15000);
                    timer.Elapsed += new ElapsedEventHandler(OutOfTime);
                    timer.AutoReset = true;
                    timer.Start();
                    break;
                default:
                    GiveHint(.75);
                    break;
            }
            hint++;
        }
        void OutOfTime(object ender, ElapsedEventArgs args) {
            answerable = false;
            Msg("Time's up! Sorry, no one guessed: {0}",currentQuestion.Answers[0]);
            if (timer.Enabled)
                timer.Stop();
            timer = new System.Timers.Timer(5000);
            timer.Elapsed += (sender, targs) => Ask();
            timer.AutoReset = true;
            timer.Start();
        }
        /// <summary>
        /// Moves to the next question.
        /// </summary>
        public void Next() {
            if (!answerable || currentQuestion == null)
                return;
            answerable = false;
            Msg("Question skipped, the correct answer was \"{0}\"", currentQuestion.Answers[0]);
            if (timer.Enabled)
                timer.Stop();
            timer = new System.Timers.Timer(2500);
            timer.Elapsed += (sender, targs) => Ask();
            timer.AutoReset = true;
            timer.Start();
        }
        void GiveHint(double percentage) {
            Msg("Hint {0}: {1}", hint, currentQuestion.GetHint(percentage));
        }
        void Msg(string message) {
            IRC.Client.RfcPrivmsg(Chan,message);
        }
        void Msg(string format, params object[] o) {
            Msg(String.Format(format, o));
        }
        /// <summary>
        /// Compares this guess with the anwser
        /// </summary>
        /// <param name="nick">User who guessed</param>
        /// <param name="quess">The user's guess</param>
        public void Guess(string nick, string attempt) {
            if (!answerable) 
                return;
            foreach (string answer in currentQuestion.Answers) {
                int ld = Util.Instance.Levenshtein(answer.ToLower(),attempt.ToLower());
                float max_error;
                if (answer.Length < 10)
                    max_error = answer.Length * (2.0f / 5.0f);
                else
                    max_error = answer.Length * (1.0f / 5.0f);

                if (ld == 0) {
                    Correct(nick, answer);
                    break;
                }
                else if (ld < max_error) {
                    if (ld > 1)
                        IRC.Client.RfcNotice(nick, String.Format("Your answer has {0} errors: \"{1}\"", ld, attempt));
                    else if (answer.Length > 1 && String.Compare(answer.Substring(0, answer.Length - 1), attempt, true) != 0)
                        IRC.Client.RfcNotice(nick, String.Format("Your answer only has one error: \"{0}\"", attempt));
                }
            }
        }
        /// <summary>
        /// Called when a user gets the correct answer
        /// </summary>
        /// <param name="nick">The user who got the answer</param>
        /// <param name="answer">The correct quess</param>
        void Correct(string nick, string answer) {
            timer.Stop();
            answerable = false;
            timer = new System.Timers.Timer(2500);
            timer.Elapsed += (sender,targs) => Ask();
            timer.AutoReset = true;
            timer.Start();
            Msg("Correct answer from {0}! Answer: {1}", nick, answer);
            if (currentQuestion.Answers.GetLength(0) > 1) {
                foreach (string possible in currentQuestion.Answers)
                    if (!possible.Equals(answer))
                        Msg("Another answer was: {0}",possible);
            }
            if (currentQuestion.IsYear)
                IncreaseScore(nick, 10);
            else {
                switch (hint) {
                    case 1:
                        IncreaseScore(nick, 5);
                        break;
                    case 2:
                        IncreaseScore(nick, 4);
                        break;
                    default:
                        IncreaseScore(nick, 3);
                        break;
                }
            }
            PrintScores();
        }
        void IncreaseScore(string nick, ushort ammount) {
            lock (scores) {
                foreach (KeyValuePair<string, ushort> kv in scores) {
                    if (String.Compare(kv.Key, nick, true) == 0) {
                        ammount += kv.Value;
                        scores.Remove(kv.Key);
                        break;
                    }
                }

                scores.Add(nick, ammount);
            }
        }
        /// <summary>
        /// Stops the current game by aborting its thread.
        /// </summary>
        public void StopGame() {
            if (!stopped) {
                stopped = true;
                Msg("Stopping trivia after current question. Use the \"start\" command to resume before it stops.");
            }
        }
        /// <summary>
        /// Resumes a stopped game.
        /// </summary>
        public void Resume() {
            if (stopped) {
                stopped = false;
                Msg("Trivia has been resumed as normal.");
            }
        }
        /// <summary>
        /// Prints scores using Msg(String, String)
        /// </summary>
        public void PrintScores() {
            lock (scores) {
                if (scores.Count == 0)
                    return;
                StringBuilder sb = new StringBuilder("Current Scores: ");
                foreach (KeyValuePair<string, ushort> kv in scores.OrderBy((kv) => ((KeyValuePair<string, ushort>)kv).Value, new ScoreComparer())) {
                    sb.AppendFormat("{0} - {1}; ", kv.Key, kv.Value);
                }
                Msg(sb.ToString(0, sb.Length - 2));
            }
        }
        public void OnNickChange(object sender, Meebey.SmartIrc4net.NickChangeEventArgs args) {
            OnNickChange(args.OldNickname, args.NewNickname);
        }
        /// <summary>
        /// Call this when a user changes his nick
        /// </summary>
        /// <param name="nick"> The old nick name</param>
        /// <param name="newnick">The new nick name</param>
        public void OnNickChange(string nick, string newnick ) {
            lock (scores) {
                bool nick_exists = false;
                ushort nick_score = 0;
                foreach (KeyValuePair<string, ushort> kv in scores) {
                    if (String.Compare(kv.Key, nick, true) == 0) {
                        scores.Remove(kv.Key);
                        nick_exists = true;
                        nick_score = kv.Value;
                        break;
                    }
                }
                if (nick_exists)
                    scores.Add(newnick, nick_score);
            }
            IRC.Client.RfcNotice(newnick, "Warning: Your name has been changed. Your score will be updated with your new name however once this game ends, your overall score (!score) will be added to THIS name, not " + nick + ". Change back if you do not want this.");
        }
        private void SaveScores() {
            try {
                MySqlCommand command = Database.Connection.CreateCommand();
                command.CommandText = "UPDATE scores SET `score`=`score` + @score WHERE `nick`=@nick AND `channel`=@chan";
                command.Prepare();
                int i = 0;
                KeyValuePair<string, ushort>[] newPlayers = new KeyValuePair<string, ushort>[scores.Count];
                lock (scores) {
                    foreach (KeyValuePair<string, ushort> kv in scores) {
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@nick", kv.Key);
                        command.Parameters.AddWithValue("@score", kv.Value);
                        command.Parameters.AddWithValue("@chan", Chan);
                        try {
                            if (command.ExecuteNonQuery() == 0)
                                newPlayers[i++] = kv;
                        }
                        catch (MySqlException) {
                            newPlayers[i++] = kv;
                        }
                    }
                }
                command.CommandText = "INSERT INTO `scores` (`nick`, `channel`, `score`) VALUES (@nick, @chan, @score)";
                command.Prepare();
                foreach (KeyValuePair<string, ushort> kv in newPlayers.Where(source => source.Value != 0 && source.Key != null)) {
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@nick", kv.Key);
                    command.Parameters.AddWithValue("@chan", Chan);
                    command.Parameters.AddWithValue("@score", kv.Value);
                    try {
                        command.ExecuteNonQuery();
                    }
                    catch (MySqlException) { }
                }
            }
            catch (Exception ex) {
                Console.WriteLine("Uncaught MySql score exception: {0}", ex.GetBaseException());
            }
        }
    }
    class ScoreComparer : IComparer<ushort> {
        public int Compare(ushort one, ushort two) {
            if (one > two)
                return -1;
            else if (one < two)
                return 1;
            else
                return 0;
        }
    }
}
