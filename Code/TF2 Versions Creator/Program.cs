﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
namespace TF2_Versions_Creator
{
    using System;
    using System.IO;
    using System.Text;

    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var t_crc  = new Crc16Ccitt(InitialCrcValue.Zeros);
                var writer = new StreamWriter(new FileStream("versions.txt", FileMode.Create), Encoding.ASCII);

                foreach (var file in Directory.GetFiles(".", "*", SearchOption.AllDirectories))
                {
                    if (Path.GetFileName(file).Equals("TF2 Versions Creator.exe")
                        || Path.GetFileName(file).Equals("versions.txt")
                        || !Equals(Path.GetDirectoryName(file), Path.GetDirectoryName("./plugins"))
                        || Path.GetExtension(file).Equals(".conf"))
                    {
                        continue;
                    }
                    writer.Write(file);
                    writer.Write((char)59);
                    writer.WriteLine(t_crc.ComputeChecksum(File.ReadAllBytes(file)));
                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error {0}", e);
            }
        }
    }
}
