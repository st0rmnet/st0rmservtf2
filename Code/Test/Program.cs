﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
namespace Test {
    class Program {
        static void Main(string[] args) {
            var text = @"Team ""Blue"" triggered ""pointcaptured"" (cp ""2"") (cpname ""#Pipeline_cap_1_blue"") (numcappers ""2"") (player1 ""dr. feelgood<100><STEAM_0:1:6253416><Blue>"") (position1 ""-1495 -1198 64"") (player2 ""Zero<101><STEAM_0:0:25532317><Blue>"") (position2 ""-1533 -1041 64"")";

            foreach
            (
                Match bracketed
                in Regex.Matches(text, @"\((?<name>[a-z0-9]+)\s+\""(?<value>[^\""]+)\""\)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace)
            )
            {
                Console.WriteLine(bracketed.Groups["name"] + " : " + bracketed.Groups["value"]);
            }
            Console.ReadLine();
        }
    }
}
