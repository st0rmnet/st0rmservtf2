﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
namespace St0rm.TF2.Launcher
{
    using System;
    using System.IO;

    public class Program
    {
        public static void Main(string[] args)
        {
            if (File.Exists("update.bat"))
            {
                File.Delete("update.bat");
            }

            Console.Title = "St0rmServ - TF2 Edition - www.St0rm.net";
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("St0rmServ for Team Fortress 2 started.");
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine("Checking for updates...");
            UpdateChecker.Instance.GetUpdates();
            Console.ReadKey();
        }
    }
}
