﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.

#pragma warning disable 0618

namespace St0rm.TF2.Launcher
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading;

    public class UpdateChecker
    {
        // formerly was: http://www.st0rm.net/tf2serv/update/
        private const string UPDATE_DIR = "http://www.cncfps.com/tf2serv/update/";

        public static UpdateChecker Instance = new UpdateChecker();

        public void GetUpdates()
        {
            var client = new WebClient();

            try
            {
                Stream instream = client.OpenRead(UPDATE_DIR + "versions.txt");
                var reader = new StreamReader(instream, Encoding.ASCII);
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string file = line.Split((char)59)[0];
                    ushort crc  = ushort.Parse(line.Split((char)59)[1]);

                    if (!File.Exists(file))
                    {
                        File.Create(file);
                        Directory.CreateDirectory(Path.GetDirectoryName(file));
                        Update(file, crc);
                    }
                    else if (GetCRC(file) != crc)
                        Update(file, crc);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error, unable to update: {0}", e);
            }
        }

        public ushort GetCRC(string file)
        {
            return new Crc16Ccitt(InitialCrcValue.Zeros).ComputeChecksum(File.ReadAllBytes(file));
        }

        private static void Update(string file, ushort crc)
        {
            try
            {
                file = file.Replace('\\', '/');
                Console.Write("Downloading {0} (  0%)...", file);
                Console.CursorLeft -= 5;

                var client  = new WebClient();
                string temp = Path.GetTempFileName();
                var mutex   = new Object();
                bool done   = false;
                Thread t    = Thread.CurrentThread;

                client.DownloadFileCompleted += (x, y) =>
                                                {
                                                    if (Path.GetFileName(file).Equals("TF2 Launcher.exe"))
                                                        UpdateLauncher();
                                                    else
                                                    {
                                                        lock (mutex)
                                                        {
                                                            done = true;
                                                            Console.CursorLeft -= 3;
                                                            Console.WriteLine("100%)...");
                                                            Console.CursorLeft = 0;
                                                            Console.Write("Installing {0}", file);

                                                            if (File.Exists(file))
                                                            {
                                                                File.Delete(file);
                                                            }
                                                            
                                                            File.Move(temp, file);
                                                            Console.CursorLeft = 0;
                                                            Console.WriteLine("Updated file: {0}", file);
                                                            t.Resume();
                                                        }
                                                    }
                                                };

                client.DownloadProgressChanged += (x, args) =>
                                                  {
                                                      lock (mutex)
                                                      {
                                                          if (done)
                                                          {
                                                              return;
                                                          }

                                                          Console.CursorLeft -= 3;
                                                          Console.Write("{0,3}", args.ProgressPercentage);
                                                      }
                                                  };

                string uri = Path.Combine(UPDATE_DIR, "uploads", file);
                
                client.DownloadFileAsync(new Uri(uri), temp);
                t.Suspend();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to update {0}: {1}", file, e);
            }
        }

        private static void UpdateLauncher()
        {
            var writer = new StreamWriter(File.OpenWrite("update.bat"));
            writer.WriteLine(@"move /Y ""{0}"" ""./TF2 Launcher.exe""");
            writer.WriteLine("\"TF2 Launcher.exe\"");
            writer.Flush();
            writer.Close();

            Process.Start("update.bat");
            Environment.Exit(0);
        }
    }
}
