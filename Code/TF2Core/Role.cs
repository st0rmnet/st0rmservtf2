﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace St0rm.TF2.Core {
    /// <summary>
    /// Represents a class role
    /// </summary>
    public class Role {
        static List<Role> roleList = new List<Role>();

        string _name, _display, _article;
        /// <summary>
        /// The display name of this role as defined in role_list
        /// </summary>
        public string Display { get { return _display; } }
        /// <summary>
        /// The TF2 name for this item, not the display name
        /// </summary>
        public string Name { get { return _name; } }
        /// <summary>
        /// Gets or sets the article for this item, ie: "an" or "a" based on vowel starting letters.
        /// You can change this to whatever you wish should the need arise
        /// </summary>
        public string Article {
            get {
                return _article;
            }
            set {
                _article = value;
            }
        }
        static Role() {
            BuildList();
        }
        private Role(string name, string display) {
            _name = name;
            _display = display;
            if (System.Text.RegularExpressions.Regex.IsMatch(display, "(?i)^[aeiou]"))
                _article = "an";
            else
                _article = "a";
        }
        /// <summary>
        /// Returns the display name
        /// </summary>
        /// <returns>Display name</returns>
       override public string ToString() {
            return _display;
        }
        /// <summary>
        /// Gets the role with the specified name
        /// </summary>
        /// <param name="name">name of role to get a Role object for</param>
        /// <returns>
        /// <list type="bullet">
        /// <item>Role created from role_list table <b>if</b> it exists</item>
        /// <item>Role created just for this object with display name Unknown (name) <b>if</b> it does not exist in the role_list table</item>
        /// </list>
        /// </returns>
       public static Role GetRole(string name) {
           if (name.Equals(""))
               return new Role("", "No Role");
           lock (roleList) {
               foreach (Role o in roleList) {
                   if (String.Compare(o._name, name, true) == 0)
                       return o;
               }
           }
           return new Role(name,String.Format("Unknown Role ({0})",name)); ;
       }
        /// <summary>
        /// (Re-)Downloads the list from the role_list table
        /// </summary>
       public static void BuildList() {
           lock (roleList) {
               roleList.Clear();
               MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
               cmd.CommandText = "SELECT * FROM role_list LIMIT 200";
               cmd.Prepare();
               MySqlDataReader reader = cmd.ExecuteReader();
               while (reader.Read()) {
                   Role r = new Role(reader.GetString(0), reader.GetString(1));
                   roleList.Add(r);
               }
               reader.Close();
           }
       }
    }
}
