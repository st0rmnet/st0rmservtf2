﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace St0rm.TF2.SSQWrapper {
    class LogListener {
        ushort _port;
        SSQ _ssq;
        bool _stop = false;
        bool _restart = false;
        DateTime _last_reply = new DateTime();

        public LogListener(ushort port, SSQ ssq) {
            _port = port;
            _ssq = ssq;
        }
        public void Start(object handlerO) {
            _stop = _restart = false;
            SSQ.LogHandler handler = (SSQ.LogHandler)handlerO;
            UdpClient client = new UdpClient(_port);
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, _port);
            _last_reply = DateTime.Now;
            byte[] buffer;
            try {
                while (!_stop && !_restart) {
                    if ((DateTime.Now - _last_reply).Minutes >= 60) {
                        _restart = true;
                        break;
                    }
                    Thread.Sleep(100);
                    buffer = client.Receive(ref ep);
                    
                    handler(Encoding.Default.GetString(buffer, 7, buffer.Length - 9));
                }
            }
            catch (ThreadAbortException) {
                Console.WriteLine("Log read thread aborted");
            }
            client.Close();
            if (!_stop && _restart) {
                SSQ.Instance.EnableLogs(St0rm.TF2.Core.Config.LogHost, St0rm.TF2.Core.Config.LogPort);
            }
        } 
    }
}
