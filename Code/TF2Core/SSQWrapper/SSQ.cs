﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
namespace St0rm.TF2.SSQWrapper
{
    using System;
    using System.Runtime.InteropServices;
    using System.Threading;
    using Core;

    /// <summary>
    ///     Wrapper for SSQ.dll
    /// </summary>
    public class SSQ
    {
        /// <summary>
        ///     Called when the server replies to something.
        /// </summary>
        /// <param name="type">Type of reply</param>
        /// <param name="reply">The reply</param>
        /// <returns>True to get all messages false to break</returns>
        public delegate bool InternalCallback(Reply_Types type, IntPtr reply);

        /// <summary>
        ///     Handles events where a new log line is received
        /// </summary>
        /// <param name="line">The log line</param>
        public delegate void LogHandler(string line);

        /// <summary>
        ///     Handles events where an Rcon Reply is received
        /// </summary>
        /// <param name="replyString">The reply</param>
        public delegate void RconReplyHandler(string replyString);

        private static readonly SSQ instance = new SSQ();

        /// <summary>
        ///     All methods using RCON automatically lock on this mutex
        ///     It can be locked in order to suspend rcon calls but it is highly discouraged
        /// </summary>
        public readonly Mutex _mutex = new Mutex(false, "Rcon");

        private string _game_server;
        private bool _hide_next_rcon = false;
        private DateTime _last_rcon_reply = new DateTime(0);

        private InternalCallback callback;

        private SSQ()
        {
            callback = new InternalCallback(InternalCallbackHandler);
            if (!SSQ_Initialize(false))
                throw new RequestFailedException();

            try
            {
                if (!SSQ_SetCallback(callback))
                    throw new RequestFailedException();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.GetBaseException());
                Console.ReadKey();
            }
        }

        /// <summary>
        ///     Gets the time of the last rcon reply from the server. Used to check heartbeat.
        /// </summary>
        public DateTime LastRconReply
        {
            get { return _last_rcon_reply; }
        }

        /// <summary>
        ///     Singleton instance
        /// </summary>
        public static SSQ Instance
        {
            get { return instance; }
        }

        /// <summary>
        ///     Gets or sets the RCON password for this instance
        /// </summary>
        public string RCONPassword { get; set; }

        /// <summary>
        ///     Gets the game server set to this instance
        /// </summary>
        public string GetGameServer
        {
            get { return _game_server; }
        }

        private bool InternalCallbackHandler(Reply_Types type, IntPtr reply)
        {
            IntPtr ptr = Marshal.ReadIntPtr(reply);

            switch (type)
            {
                case Reply_Types.SSQ_RCON_REPLY_CALLBACK:

                    string rcon = Marshal.PtrToStringAnsi(ptr);

                    _last_rcon_reply = DateTime.Now;

                    if (OnRconReply != null)
                        OnRconReply(rcon);

                    if (!_hide_next_rcon)
                        IRC.Instance.Broadcast(rcon);
                    else
                        _hide_next_rcon = false;

                    break;
                case Reply_Types.SSQ_RULES_REPLY_CALLBACK:
                    var rules = (Rules_Reply)Marshal.PtrToStructure(ptr, typeof (Rules_Reply));
                    break;
                case Reply_Types.SSQ_LOG_REPLY_CALLBACK:
                    string line = Marshal.PtrToStringAnsi(ptr);
                    if (OnLog != null)
                        OnLog(line);
                    break;
            }
            return true;
        }

        [DllImport("SSQ.dll", EntryPoint = "SSQ_FormatBatchReply")]
        private static extern string SSQ_FormatBatchReply(Batch_Reply reply, int index);

        [DllImport("SSQ.dll", EntryPoint = "SSQ_GetBatchReply")]
        private static extern bool SSQ_GetBatchReply(byte region, string filter);

        [DllImport("SSQ.dll", EntryPoint = "SSQ_GetInfoReply")]
        private static extern bool SSQ_GetInfoReply(ref Info_Reply reply);

        [DllImport("SSQ.dll", EntryPoint = "SSQ_SetCallbackAddress")]
        private static extern bool SSQ_SetCallback(InternalCallback callback);

        [DllImport("SSQ.dll", EntryPoint = "SSQ_SetGameServer")]
        private static extern bool SSQ_SetGameServer(string address);

        [DllImport("SSQ.dll", EntryPoint = "SSQ_Initialize")]
        private static extern bool SSQ_Initialize(bool stop);

        [DllImport("SSQ.dll", EntryPoint = "SSQ_GetRconReply")]
        private static extern bool SSQ_Rcon(string pass, string command);

        [DllImport("SSQ.dll", EntryPoint = "SSQ_GetRulesReply")]
        private static extern bool SSQ_GetRulesReply();

        [DllImport("SSQ.dll", EntryPoint = "SSQ_GetPlayerReply")]
        private static extern bool SSQ_GetPlayerReply(ref Player_Reply reply);

        /// <summary>
        ///     I can not make this work with TF2. As such, SetLogStatus has been changed in this wrapper to add and check itself.
        /// </summary>
        [DllImport("SSQ.dll", EntryPoint = "SSQ_SetLogStatus")]
        private static extern bool SSQ_SetLogStatus(bool status, ushort port);

        /// <summary>
        ///     Called when an Rcon Reply is received
        /// </summary>
        public event RconReplyHandler OnRconReply;

        /// <summary>
        ///     Triggers when a new log line is received
        /// </summary>
        public event LogHandler OnLog;

        /// <summary>
        ///     Formats a raw address into a dotted string address
        /// </summary>
        /// <param name="reply">Structure to format</param>
        /// <param name="index">index of the raw address to format</param>
        /// <returns>Formatted raw address</returns>
        public string FormatBatchReply(Batch_Reply reply, int index)
        {
            return SSQ_FormatBatchReply(reply, index);
        }

        /// <summary>
        ///     Requests a list of game servers from a master server set with SetMasterServer
        /// </summary>
        /// <param name="region">Region of servers</param>
        /// <param name="filter">Server filter</param>
        /// <returns>Success</returns>
        public bool GetBatchReply(Region region, string filter)
        {
            return SSQ_GetBatchReply((byte)region, filter);
        }

        /// <summary>
        ///     Gets information about the server set using SetGameServer
        /// </summary>
        /// <returns>The reply</returns>
        public Info_Reply GetInfoReply()
        {
            lock (_mutex)
            {
                var reply = new Info_Reply();
                bool success = SSQ_GetInfoReply(ref reply);
                if (success)
                    return reply;
                else
                    throw new RequestFailedException();
            }
        }

        /// <summary>
        ///     Tells the bot not to message the next rcon reply to the channel, usually because you are doing something with it
        ///     and do not wish for it to flood
        /// </summary>
        public void HideNextRcon()
        {
            _hide_next_rcon = true;
        }

        /// <summary>
        ///     Gets a Player_Reply struct from SSQ.dll
        /// </summary>
        /// <returns>Player_Reply struct from SSQ.dll</returns>
        public Player_Reply GetPlayerReply()
        {
            lock (_mutex)
            {
                var reply = new Player_Reply();
                bool success = SSQ_GetPlayerReply(ref reply);

                if (success)
                    return reply;
                else
                    throw new RequestFailedException();
            }
        }

        /// <summary>
        ///     Sets the game server to be used for all commands
        ///     Connects and gets an RCON Challenge
        /// </summary>
        /// <param name="host">Host of server</param>
        /// <param name="port">Rcon Port of server</param>
        public void SetGameServer(string host, ushort port)
        {
            lock (_mutex)
            {
                if (!SSQ_SetGameServer(String.Format("{0}:{1}", host, port)))
                    throw new RequestFailedException();
                else
                    _game_server = host;
            }
        }

        /// <summary>
        ///     Executes a raw rcon command
        /// </summary>
        /// <param name="command">Command to execute</param>
        /// <returns>Success</returns>
        public bool Rcon(string command)
        {
            lock (_mutex)
            {
                return SSQ_Rcon(RCONPassword, command);
            }
        }

        /// <summary>
        ///     This function calls SSQ_SetLogStatus,
        ///     <b>
        ///         however since I cannot get it to work with TF2, I have added
        ///         EnableLogs and DisableLogs
        ///     </b>
        /// </summary>
        /// <param name="enabled">To enable or disable logs</param>
        /// <param name="port">Port to listen for logs on</param>
        public void SetLogStatus(bool enabled, ushort port)
        {
            if (!SSQ_SetLogStatus(enabled, port))
                throw new RequestFailedException();
        }

        /// <summary>
        ///     Enables logs
        /// </summary>
        /// <param name="externalHost">Your external IP address</param>
        /// <param name="port">The port to listen for logs on</param>
        public void EnableLogs(string externalHost, ushort port)
        {
            string addLine = String.Format("logaddress_add {0}:{1}", externalHost, port);
            if (!Rcon("log on") || !Rcon(addLine))
                throw new RequestFailedException();
            var listener = new LogListener(port, this);
            new Thread(new ParameterizedThreadStart(listener.Start)).Start(new LogHandler(x =>
                                                                                          {
                                                                                              if (OnLog != null)
                                                                                              {
                                                                                                  OnLog(x);
                                                                                              }
                                                                                          }));
        }

        /// <summary>
        ///     Disables logging NOT IMPLEMENTED
        /// </summary>
        /// <param name="port">Port to disable logging on</param>
        public void DisableLogs(ushort port)
        {
        }

        /// <summary>
        ///     Asycnhronisly gets a Rules Reply from the server (Called when received via an event)
        /// </summary>
        public void GetRulesReply()
        {
            lock (_mutex)
            {
                if (!SSQ_GetRulesReply())
                    throw new RequestFailedException();
            }
        }
    }

    /// <summary>
    ///     Thrown when an SSQ.dll method reports failure
    /// </summary>
    public class RequestFailedException : Exception
    {
    }

    /// <summary>
    ///     Type of possible callback replies
    /// </summary>
    public enum Reply_Types
    {
        /// <summary>
        ///     This is a batch reply
        /// </summary>
        SSQ_BATCH_REPLY_CALLBACK = 0,

        /// <summary>
        ///     This is a log reply
        /// </summary>
        SSQ_LOG_REPLY_CALLBACK,

        /// <summary>
        ///     This is an rcon reply
        /// </summary>
        SSQ_RCON_REPLY_CALLBACK,

        /// <summary>
        ///     This is a rules reply
        /// </summary>
        SSQ_RULES_REPLY_CALLBACK,

        /// <summary>
        ///     This is a log reply
        /// </summary>
        SSQ_LOG_THREAD_NOTIFY
    }

    /// <summary>
    ///     Region for bath replies
    /// </summary>
    public enum Region
    {
#pragma warning disable 1591
        SSQ_GS_TIMEOUT = 1 << 0,
        SSQ_LOG_TIMEOUT = 1 << 1,
        SSQ_MS_TIMEOUT = 1 << 2,
        SSQ_RCON_TIMEOUT = 1 << 3
    }

    /// <summary>
    ///     Location for batch replies
    /// </summary>
    public enum Location
    {
        SSQ_USA_EAST = 0,
        SSQ_USA_WEST,
        SSQ_SOUTH_AMERICA,
        SSQ_EUROPE,
        SSQ_ASIA,
        SSQ_AUSTRALIA,
        SSQ_MIDDLE_EAST,
        SSQ_AFRICA,
        SSQ_WORLD = 255
    }
#pragma warning restore 1591
    /// <summary>
    ///     Batch_Reply for SSQ.dll
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Batch_Reply
    {
        /// <summary>
        ///     Number of game server addressess
        /// </summary>
        private readonly long num_servers;

        /// <summary>
        ///     Size of the string data, useless in C#
        /// </summary>
        private readonly long data_size;

        /// <summary>
        ///     Raw server addressess
        /// </summary>
        private readonly string data;
    }

    /// <summary>
    ///     Info_Reply struct for SSQ.dll
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct Info_Reply
    {
        /// <summary>
        ///     The network protocol version
        /// </summary>
        public char version;

        /// <summary>
        ///     Game server hostname
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)] public string hostname;

        /// <summary>
        ///     Current map
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)] public string map;

        /// <summary>
        ///     Game directory for the server
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)] public string game_directory;

        /// <summary>
        ///     Server's game description
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)] public string game_description;

        /// <summary>
        ///     Steam application identifier
        /// </summary>
        public short app_id;

        /// <summary>
        ///     The number of players in the server
        /// </summary>
        public byte num_players;

        /// <summary>
        ///     The max number of players in the server
        /// </summary>
        public byte max_players;

        /// <summary>
        ///     The number of bots in the server
        /// </summary>
        public byte num_of_bots;

        /// <summary>
        ///     Value is "d" for dedicated server and "l" for listen server
        /// </summary>
        public char dedicated;

        /// <summary>
        ///     Value is 'l' for Linux and 'w' for Windows. OS running on the game server.
        /// </summary>
        public char os;

        /// <summary>
        ///     1 if the server is password protected
        /// </summary>
        public byte password;

        /// <summary>
        ///     1 if anti-cheat
        /// </summary>
        public byte secure;

        /// <summary>
        ///     Version of the game
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)] public string game_version;
    }

    /// <summary>
    ///     Player_Item reply struct for SSQ.dll
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct Player_Item
    {
        /// <summary>
        ///     Slot player is in
        ///     Can be -1 or 0 for a connecting player
        /// </summary>
        public byte index;

        /// <summary>
        ///     Name of the player
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)] public string player_name;

        /// <summary>
        ///     Number of kills the player has
        /// </summary>
        public int kills;

        /// <summary>
        ///     Time since player joined server
        /// </summary>
        public float time_connected;
    }

    /// <summary>
    ///     Player_Reply struct for SSQ.dll
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct Player_Reply
    {
        /// <summary>
        ///     The number of players on the server and in this reply
        /// </summary>
        public byte num_players;

        /// <summary>
        ///     The players ingame
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)] public Player_Item[] players;
    }

    /// <summary>
    ///     Rules_Reply struct for SSQ.dll
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Rules_Reply
    {
        /// <summary>
        ///     Number of rules in the reply
        /// </summary>
        public short num_rules;

        /// <summary>
        ///     Size of data
        /// </summary>
        public long data_size;

        /// <summary>
        ///     List of rules delimated by a null character
        /// </summary>
        public string data;
    }
}
