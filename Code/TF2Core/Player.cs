﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.SSQWrapper;

namespace St0rm.TF2.Core {
    /// <summary>
    /// Represents a Player in the game
    /// </summary>
    public class Player {
        static List<Player> _players = new List<Player>();
        /// <summary>
        /// The Player which is used to represent the console
        /// </summary>
        public static readonly Player CONSOLE = new Player(0, "0", "Console");

        #region Private Members
        int _id = 0;
        string _name = null;
        string _steamId = null;
        Team _team = Team.UNKNOWN;
        Role _role = Role.GetRole("Unknown");
        System.Net.IPAddress _host = null;
        string _ip = "Unknown";
        ushort _port = 0;
        float _stay = 0f;
        PlayerAccess _access = null;
        int _access_lookup_attempts = 0;
        #endregion
        #region Properties
        /// <summary>
        /// Gets and the player's ID
        /// </summary>
        public int Id { get { return _id; } set { _id = value; } }
        /// <summary>
        /// Gets and sets the players Name
        /// </summary>
        public string Name { get { return _name; } set { _name = value; } }
        /// <summary>
        /// Gets and sets the players Access as defined in the database
        /// </summary>
        public PlayerAccess Access { 
            get {
                try {
                    if (_access != null) {
                        _access_lookup_attempts = 0;
                        return _access;
                        
                    }
                    else if (_id == 0 && (_steamId == null || _steamId.Equals(""))) {
                        _access_lookup_attempts = 0;
                        return PlayerAccess.DEFAULT;
                        
                    }
                    else {
                        PlayerAccess a = PlayerAccess.GetAccess(this);
                        _access = a;
                        _access_lookup_attempts = 0;
                        return a;
                    }
                }
                catch (Exception e) {
                    if (_access_lookup_attempts >= 5)
                        throw e;
                    else {
                        _access_lookup_attempts++;
                        return Access;
                    }
                }
            }
            set {
                _access = value;
            }
        }
        /// <summary>
        /// Gets the name of the player colored for his team
        /// </summary>
        public string ColoredName {
            get {
                if (this == CONSOLE)
                    return "03Console";
                return _team.GetColor(_name);
            }
        }
        /// <summary>
        /// Gets the possessive termed name for the player, for example:
        /// aca20031 = aca20031's
        /// WiloS = WiloS'
        /// </summary>
        public string PossessiveName {
            get {
                if (_name.EndsWith("s", true, null))
                    return _name + "'";
                else
                    return _name + "'s";
            }
        }
        /// <summary>
        /// The Steam ID for this person, returns Unknown if it is an empty unset string
        /// </summary>
        public string SteamID { 
            get {
                if (_steamId == null || _steamId.Equals(""))
                    return "Unknown";
                else
                    return _steamId; 
            }
            set { _steamId = value; } }
        /// <summary>
        /// Gets and sets the Team this player is on
        /// </summary>
        public Team Team { get { return _team; } set { _team = value; } }
        /// <summary>
        /// Gets and sets the Role (Class) this player has (If it was seen)
        /// </summary>
        public Role Role { get { return _role; } set { _role = value; } }
        /// <summary>
        /// Gets and sets this players IP address independantly from the host
        /// </summary>
        public String IP { 
            get {
                return _ip;
            } 
            set {
                _ip = value;
            }
        }
        /// <summary>
        /// Gets and sets the IPAddress of this player
        /// null if the DNS could not be resolved properly
        /// IP can be non-empty if Host is null
        /// <b>Note:</b> Even if Host is null, the will IP can be valid if the user is fully connected (id != -1)
        /// </summary>
        public System.Net.IPAddress Host { 
            get { 
                return _host; 
            } set { 
                _host = value;

                if (value != null && IP.Equals("Unknown"))
                    IP = _host.ToString();
            } 
        }
        /// <summary>
        /// Gets and sets the port which this player is connected from
        /// </summary>
        public ushort Port { get { return _port; } set { _port = value; } }
        /// <summary>
        /// How many seconds this player has been in the server
        /// </summary>
        public float Stay {
            get {
                return _stay;
            }
            set {
                _stay = value;
                StayLastUpdated = System.DateTime.Now.Ticks;
            } 
        }
        /// <summary>
        /// Gets and Sets the DateTime.Ticks when this player's time ingame was last set
        /// Should not need to be changed and is used to calculate stay time dynamically to the milisecond
        /// </summary>
        public long StayLastUpdated { get; set; }
        /// <summary>
        /// Gets the TimeSpan which this player has spent in the game
        /// </summary>
        public TimeSpan StaySpan {
            get {
                Stay += (System.DateTime.Now.Ticks - StayLastUpdated) / 1000f / 10000f;
                return new TimeSpan((long)(Stay * 10000000));
            }
        }

        /// <summary>
        /// Number of kills this player has
        /// </summary>
        public long Kills { get; set; }
        #endregion
        #region Constructors and Destructors
        /// <summary>
        /// Creates a new Player
        /// </summary>
        /// <param name="id">Players ID</param>
        /// <param name="steamId">Players Steam ID</param>
        /// <param name="name">Players Name</param>
        public Player(int id, string steamId, string name) {
            _id = id;
            _steamId = steamId;
            _name = name;

        }
        /// <summary>
        /// Copy Constructor
        /// </summary>
        public Player(Player old) {
            _id = old.Id;
            _name = old.Name;
            _steamId = old.SteamID;
            _team = old.Team;
            _port = old.Port;
            _host = old.Host;
            _role = old.Role;
            Stay = old.Stay;
            _access = old.Access;
        }
        /// <summary>
        /// Destructs the Player object
        /// </summary>
        ~Player() {
            lock (_players) {
                if (_players.Contains(this))
                    _players.Remove(this);
            }
        }
        #endregion
        #region Static Methods
        /// <summary>
        /// Adds a player to the list of ingame players if an equal object does not exist
        /// Equal objects are detmined by == and Equals
        /// </summary>
        /// <param name="p">Player to add</param>
        /// <returns>Success</returns>
        public static bool AddPlayer(Player p) {
            lock (_players) {
                foreach (Player cPlayer in _players)
                    if (cPlayer == p || cPlayer.Equals(p))
                        return false;
                _players.Add(p);
            }
            return true;
        }
        /// <summary>
        /// Deletes a player from the list
        /// Equal objects are detmined by == and Equals
        /// </summary>
        /// <param name="p">Player to delete</param>
        /// <returns>Success</returns>
        public static bool DelPlayer(Player p) {
            List<Player> playersToRemove = new List<Player>();
            lock (_players) {
                return _players.RemoveAll(new Predicate<Player>(x => x == p || x.Equals(p))) > 0;
            }
        }
        /// <summary>
        /// Gets a player from the list by his ID
        /// </summary>
        /// <param name="id">Id of player to get</param>
        /// <returns>Player or null</returns>
        public static Player GetPlayer(int id) {
            if (id == 0) 
                return CONSOLE;
            lock (_players) {
                foreach (Player p in _players)
                    if (p.Id == id)
                        return p;
            }
            return null;
        }
        /// <summary>
        /// Gets a player from the list by his Name
        /// </summary>
        /// <param name="name">Name of player to get</param>
        /// <returns>Player or null</returns>
        public static Player GetPlayer(string name) {
            if (name.ToLower().Equals("console"))
                return CONSOLE;
            lock (_players) {
                foreach (Player p in _players)
                    if (String.Compare(p.Name,name,true) == 0)
                        return p;
            }
            return null;
        }
        /// <summary>
        /// Returns all players with the string in their name
        /// </summary>
        /// <param name="contains">case-insensative string</param>
        /// <returns>Player[] of matches - Can be 0 length</returns>
        public static Player[] FindPlayers(string contains) {
            List<Player> matches = new List<Player>();
            lock (_players) {
                foreach (Player p in _players)
                    if (p._name.ToLower().Contains(contains.ToLower()))
                        matches.Add(p);
            }
           Player[] pl = new Player[matches.Count];
            matches.CopyTo(pl);
            return pl;
        }
        /// <summary>
        /// Returns a <b>copy</b> of the player list.
        /// </summary>
        /// <returns><b>copy</b> of the player list.</returns>
        public static Player[] GetPlayerList() {
            Player[] pl = new Player[_players.Count];
            _players.CopyTo(pl);
            return pl;
        }
        /// <summary>
        /// Gets a <b>copy</b> of the player list of all players on the given Team
        /// </summary>
        /// <param name="team">Team of players to get</param>
        /// <returns>Player[] of all players on Team</returns>
        public static Player[] GetPlayerList(Team team) {
            List<Player> pl = new List<Player>();
            lock (_players) {
                foreach (Player p in _players)
                    if (p.Team == team)
                        pl.Add(p);
            }
            Player[] pa = new Player[pl.Count];
            pl.CopyTo(pa);
            return pa;
        }
        /// <summary>
        /// Syncs player list with that of the server's
        /// </summary>
        public static void RefreshList() {
            lock (_players) {
               try {
                   Player_Reply reply = SSQ.Instance.GetPlayerReply();
                   //Removes players not in the structure
                   List<Player> toRemove = new List<Player>();
                   foreach (Player player in _players) {
                       bool inreply = false;
                       for (int i = 0; i < reply.num_players; i++) {
                           if (String.Compare(reply.players[i].player_name, player.Name, true) == 0) {
                               inreply = true;
                               if (reply.players[i].index > 127)
                                   player.Team = Team.LOBBY;
                               break;
                           }
                       }
                       if (!inreply)
                           toRemove.Add(player);
                   }
                   _players.RemoveAll((p) => toRemove.Contains(p));
                  //Adds any player not in the list
                  for (int i = 0; i < reply.num_players; i++) {
                      if (Player.GetPlayer(reply.players[i].player_name) != null)
                          continue;
                      else if (!reply.players[i].player_name.Equals("")) {
                          Player player = new Player(-1, "", reply.players[i].player_name);
                          player.Kills += reply.players[i].kills;
                          player.Stay = reply.players[i].time_connected;
                          if (reply.players[i].index > 127)
                              player.Team = Team.LOBBY;
                          Player.AddPlayer(player);
                      }
                  }
                }
                catch (RequestFailedException) {
                   ConsoleColor c = Console.ForegroundColor;
                   Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Could not update player list manually.");
                    Console.ForegroundColor = c;
                }
            }
        }
        
       
        #endregion


        /// <summary>
        /// Returns the team-colored name for this player.
        /// </summary>
        /// <returns>Colored name</returns>
        override public string ToString() {
            return Name;
        }
        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        public void kick() {
            if (Id > 0)
                Remote.Raw("kickid {0}", Id);
            else
                Remote.Raw("kick {0}", Name);
        }
  
    }
    
}
