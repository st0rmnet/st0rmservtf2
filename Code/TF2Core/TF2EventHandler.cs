﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Net;
using St0rm.TF2.SSQWrapper;
using St0rm.TF2.Events;
using St0rm.TF2.IRCCommands;
using St0rm.TF2.Core.Delegates;
using St0rm.TF2.Core.RconParsers;

namespace St0rm.TF2.Core
{
    /// <summary>
    /// The log parser which also calls events
    /// </summary>
    public class TF2EventHandler
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static TF2EventHandler Instance = new TF2EventHandler();

        #region Events

        /// <summary>
        /// Triggered when one player kills another
        /// </summary>
        public event KillHandler OnKill;

        /// <summary>
        /// Triggered when a player changes or first selects his team.
        /// </summary>
        public event TeamChangeHandler OnTeamChange;

        /// <summary>
        /// Triggered when an object is destroyed
        /// </summary>
        public event ObjectKillHandler OnObjectKill;

        /// <summary>
        /// Triggered when a player changes or first selects his role
        /// </summary>
        public event RoleChangeHandler OnRoleChange;

        /// <summary>
        /// Triggered when a player connects to the server (Before he joins the game)
        /// </summary>
        public event ConnectHandler OnConnect;

        /// <summary>
        /// Triggers when a player speaks via text chat
        /// </summary>
        public event SayHandler OnSay;

        /// <summary>
        /// Triggers when a player enters the game (after he has connected)
        /// </summary>
        public event EnterHandler OnEnter;

        /// <summary>
        /// 
        /// Triggers when a steam user id is validated
        /// </summary>
        public event ValidatedHandler OnValidated;

        /// <summary>
        /// Triggers when a player assists another in a kill
        /// </summary>
        public event AssistHandler OnAssist;

        /// <summary>
        /// Triggers when an object is built by a player
        /// </summary>
        public event ObjectBuiltHandler OnObjectBuilt;

        /// <summary>
        /// Triggers when one player dominates another
        /// </summary>
        public event DominationHandler OnDomination;

        /// <summary>
        /// Triggers when a player takes revenge on another
        /// </summary>
        public event RevengeHandler OnRevenge;

        /// <summary>
        /// Triggers when a player disconnects from the server (leaves)
        /// </summary>
        public event DisconnectHandler OnDisconnect;

        /// <summary>
        /// Triggers when a player commits suicide
        /// </summary>
        public event SuicideHandler OnSuicide;

        /// <summary>
        /// Triggers when a charge is deployed by a player
        /// </summary>
        public event ChargeDeployedHandler OnChargeDeployed;

        /// <summary>
        /// Triggers when a player captures, defends, recovers, etc. the flag
        /// Note: Flag applies to intelligence, cart, etc.
        /// </summary>
        public event FlagEventHandler OnFlagEvent;

        /// <summary>
        /// Triggers when an RCON command is received by the server from any host
        /// (Including the bot)
        /// </summary>
        public event RconHandler OnRconReceived;

        /// <summary>
        /// Triggers when a player changes his name
        /// </summary>
        public event NameChangeHandler OnNameChange;

        /// <summary>
        /// Triggers when a round begins
        /// </summary>
        public event RoundStartHandler OnRoundStart;

        /// <summary>
        /// Triggers when a map <b>begins</b> loading
        /// </summary>
        public event MapLoadHandler OnMapLoad;

        /// <summary>
        /// Triggers when a map <b>finishes</b> loading
        /// </summary>
        public event MapStartedHandler OnMapStarted;

        /// <summary>
        /// Triggers when a round setup begins or ends. True on begin, false on end.
        /// </summary>
        public event RoundSetupHandler OnRoundSetup;

        /// <summary>
        /// Triggers when a round ends and a winner is determined. UNKNOWN means tie.
        /// </summary>
        public event RoundWinHandler OnRoundWin;

        /// <summary>
        /// Triggers when a round ends and a length is given
        /// </summary>
        public event RoundLengthHandler OnRoundLength;

        /// <summary>
        /// Triggers when a mini round starts
        /// </summary>
        public event MiniRoundStartHandler OnMiniRoundStart;

        /// <summary>
        /// Triggers when a mini-round is 'selected'. Called with the selected round ie round_1 or round_a
        /// </summary>
        public event MiniRoundSelectedHandler OnMiniRoundSelected;

        /// <summary>
        /// Triggers when a team wins a mini round
        /// </summary>
        public event MiniRoundWinHandler OnMiniRoundWin;

        /// <summary>
        /// Triggers when a mini round ends and has a determined length
        /// </summary>
        public event MiniRoundLengthHandler OnMiniRoundLength;

        /// <summary>
        /// Triggers when a player calls a command from ingame
        /// </summary>
        public event GameCommandHandler OnGameCommand;

        /// <summary>
        /// Triggers when a round ends in stalemate (tie)
        /// </summary>
        public event RoundStalemateHandler OnRoundStalemate;

        /// <summary>
        ///  Triggers when a round ends due to time running out
        /// </summary>
        public event GameTimeoutHandler OnGameTimeout;

        /// <summary>
        /// Triggers when a game ends due to the round limit being reached
        /// </summary>
        public event GameRoundLimitHandler OnGameRoundLimit;

        /// <summary>
        /// Triggers when a round enters sudden death
        /// </summary>
        public event SuddenDeathHandler OnSuddenDeath;

        #endregion

        #region Regular Expession Declarations

        private Regex rUser = new Regex("\"(.*)<(\\d+)><(STEAM_[^>]+)><([^>]+)>\"");
        private Regex rConsole = new Regex("\"Console<0><Console><Console>\"");
        private Regex rTeamlessUser = new Regex("\"(.*)<(\\d+)><(STEAM_[^>]+)><>\"");
        private Regex rOptionalTeamUser = new Regex("\"(.*)<(\\d+)><(STEAM_[^>]+)><([^>]*)>\"");

        private Regex rFlagEvent;
        private Regex rKill;
        private Regex rTeamChange;
        private Regex rKillObject;
        private Regex rBuiltObject;
        private Regex rRoleChange;
        private Regex rConnect;
        private Regex rSay;
        private Regex rConsoleSay;
        private Regex rSayTeam;
        private Regex rEnter;
        private Regex rAssist;
        private Regex rDisconnect;
        private Regex rDomination;
        private Regex rRevenge;
        private Regex rValidated;
        private Regex rSuicide;
        private Regex rNameChange;
        private Regex rChargeDeployed;
        private Regex rScoreUpdate;
        private Regex rRcon = new Regex(@"rcon from ""([^""]+)"": command ""([^""]+)""");

        //World triggered "Round_Start"
        private Regex rRoundStart = new Regex("World triggered \"Round_Start\"");

        //Loading map "ctf_well"
        private Regex rMapLoad = new Regex("^Loading map \"(.*)\"$");

        //Started map "ctf_well" (CRC "-891036843")
        private Regex rStartedMap = new Regex("^Started map \"(.*)\" \\(CRC \\S+$");

        //World triggered "Round_Setup_Begin"
        //World triggered "Round_Setup_End"
        private Regex rRoundSetup = new Regex("^World triggered \"Round_Setup_(.*)\"$");

        //World triggered "Round_Win" (winner "Blue")
        private Regex rRoundWin = new Regex(@"^World triggered ""Round_Win"" \(winner ""(.*)""\)$");

        //World triggered "Round_Length" (seconds "152.19")
        private Regex rRoundLength = new Regex(@"^World triggered ""Round_Length"" \(seconds ""(.*)""\)$");

        //World triggered "Mini_Round_Start"
        private Regex rMiniRoundStart = new Regex(@"^World triggered ""Mini_Round_Start""$");

        //World triggered "Mini_Round_Selected" (round "round_3")
        private Regex rMiniRoundSelected = new Regex(@"^World triggered ""Mini_Round_Selected"" \(round ""([^""]+)""\)$");

        //World triggered "Mini_Round_Win" (winner "Blue") (round "round_3")
        private Regex rMiniRoundWin =
            new Regex(@"^World triggered ""Mini_Round_Win"" \(winner ""([^""]+)""\) \(round ""([^""]+)""\)$");

        //World triggered "Mini_Round_Length" (seconds "796.80")
        private Regex rMiniRoundLength = new Regex(@"^World triggered ""Mini_Round_Length"" \(seconds ""([^""]+)""\)$");

        //[META] Loaded 0 plugins (1 already loaded)
        private Regex rMetaPluginLoad = new Regex(@"^\[META\] Loaded \d+ plugins.*$");

        //World triggered "Round_SuddenDeath" reason "round timelimit reached"
        private Regex rSuddenDeath = new Regex(@"^World triggered ""Round_SuddenDeath"" reason ""(.*)""$");


        #endregion

        private bool _cvar_list = false;

        private TF2EventHandler()
        {
            SSQ.Instance.OnLog += LogHandler;

            #region Regular Expression Definitions

            // 11/07/2008 - 13:59:56: "06_EMRE_61<141><STEAM_0:1:19086240><Blue>" triggered "flagevent" (event "defended") (position "-206 198 -280")
            rFlagEvent = new Regex(rUser + " triggered \"flagevent\" \\(event \"([^\"]+)\"\\)");

            //"4thForce<135><STEAM_0:1:18361525><Blue>" killed "Gawain<149><STEAM_0:1:16918722><Red>" with "sniperrifle" (customkill "headshot") (attacker_position "-6 -957 256") (victim_position "366 1127 256")
            //In Memory of SutterCane<651><STEAM_0:1:19815950><Red>" killed "BobLankin<830><STEAM_0:1:20770080><Blue>" with "" (attacker_position "-33 -1274 64") (victim_position "64 -1110 64")
            rKill = new Regex(rUser.ToString() + " killed " + rUser.ToString() + " with \"([^\"]*)\" (.*)");

            //"Cobra Dude<693><STEAM_0:0:18874021><Unassigned>" joined team "Red"
            rTeamChange = new Regex(rUser + " joined team \"([^\"]+)\"");

            //"Lloyien<701><STEAM_0:1:19997574><Blue>" triggered "killedobject" (object "OBJ_TELEPORTER_ENTRANCE") (weapon "pda_engineer") (objectowner "Lloyien<701><STEAM_0:1:19997574><Blue>") (attacker_position "-240 2980 -191")
            //   "Damon<824><STEAM_0:1:13426507><Red>" triggered "killedobject" (object "OBJ_SENTRYGUN") (objectowner "wiz999<826><STEAM_0:1:5287323><Blue>") (assist "1") 
            rKillObject =
                new Regex(rUser +
                          " triggered \"killedobject\" \\(object \"([^\"]+)\"\\) (?:\\(weapon \"([^\"]+)\"\\) )?\\(objectowner " +
                          rUser.ToString() + "\\)");

            //"anadga<817><STEAM_0:0:18613542><Red>" triggered "builtobject" (object "OBJ_DISPENSER") (position "-456 -1836 -167")
            rBuiltObject = new Regex(rUser + " triggered \"builtobject\" \\(object \"([^\"]+)\"\\)");

            //".BAD.m0nki3<713><STEAM_0:0:16677416><Blue>" changed role to "soldier"
            rRoleChange = new Regex(rUser + " changed role to \"([^\"]+)\"");

            //"SpraynPray12<730><STEAM_ID_PENDING><>" connected, address "76.180.224.160:16592"
            rConnect = new Regex(rTeamlessUser + " connected, address \"([^:]+):(\\d+)\"");

            //"Flamefacer<643><STEAM_0:1:20770114><Blue>" say "flamethrower is so incredibly noobish"
            rSay = new Regex(rUser + " say \"(.*)\"");

            //"Console<0><Console><Console>" say "test2"
            rConsoleSay = new Regex(rConsole + " say \"(.*)\"");

            //"Diesel<738><STEAM_0:0:13274793><Red>" say_team "tjere in intel"
            rSayTeam = new Regex(rUser + " say_team \"(.*)\"");

            // "lolmastr<789><STEAM_0:1:19293311><>" entered the game
            rEnter = new Regex(rTeamlessUser + " entered the game");

            //"Folklore<771><STEAM_0:0:5565737><Red>" triggered "kill assist" against "Gusfacer<726><STEAM_0:1:19495942><Blue>" (assister_position "287 958 256") (attacker_position "25 -241 12") (victim_position "-449 -1076 279")
            rAssist = new Regex(rUser + " triggered \"kill assist\" against " + rUser);

            //"mopedguy<1341><STEAM_0:0:5225371><Blue>" disconnected (reason "No Steam logon
            //"KAMIKAZA!!<807><STEAM_0:0:14715043><Unassigned>" disconnected (reason "Disconnect by user.")
            // "Setheen<883><STEAM_0:0:12305610><>" disconnected (reason "Disconnect by user.")
            rDisconnect = new Regex(rOptionalTeamUser + @" disconnected \(reason ""(.*?)(?:""\))?$");

            //"lolmastr<789><STEAM_0:1:19293311><Red>" triggered "domination" against "sandworm508<801><STEAM_0:1:18130563><Blue>" (assist "1")
            rDomination =
                new Regex(rUser + " triggered \"domination\" against " + rUser + "(?: \\(assist \"(\\d+)\"\\))?");

            //"billbobjobriggs<795><STEAM_0:1:15138936><Red>" triggered "revenge" against "JIGGLYPUFF the Anti-Hero<820><STEAM_0:0:19572811><Blue>" (assist "1")
            rRevenge = new Regex(rUser + " triggered \"revenge\" against " + rUser + "(?: \\(assist \"(\\d+)\"\\))?");

            //"Your worst nightmare<857><STEAM_0:1:17465315><>" STEAM USERID validated
            rValidated = new Regex(rTeamlessUser + " STEAM USERID validated");

            //"Blackdog II<866><STEAM_0:0:20321118><Red>" committed suicide with "world" (attacker_position "-21 -1368 64")
            //"Krunkd<827><STEAM_0:0:2358017><Blue>" committed suicide with "tf_projectile_pipe" (attacker_position "-362 -1096 256")
            rSuicide = new Regex(rUser + " committed suicide with \"([^\"]+)\"");

            //"niclerma<2114><STEAM_0:0:3422240><Blue>" changed name to "Mr.Tinkles"
            rNameChange = new Regex(rUser + @" changed name to ""([^""]+)""$");

            //"noBesh<856><STEAM_0:0:16674250><Red>" triggered "chargedeployed"
            rChargeDeployed = new Regex(rUser + " triggered \"chargedeployed\"");

            //Team "Red" current score "0" with "0" players
            rScoreUpdate = new Regex(@"^Team ""([^""]+)"" (?:final|current) score ""(\d+)"" with ""(\d+)"" players$");

            #endregion
        }
#pragma warning disable 0168
        /// <summary>
        /// Initializes all core events by creating their singleton instance
        /// Only works once (Calls static constructors)
        /// </summary>
        public static void InitializeEvents()
        {
            TF2EventHandler evth = TF2EventHandler.Instance;
            EKills ekills        = EKills.Instance;
            ESay esay            = ESay.Instance;
            EJoinLeave ejl       = EJoinLeave.Instance;
            ETeamChange etc      = ETeamChange.Instance;
            ERevengeDominate erd = ERevengeDominate.Instance;
            EAssistSuicide eas   = EAssistSuicide.Instance;
            EObjects eo          = EObjects.Instance;
            ERoleChange erc      = ERoleChange.Instance;
            EFlag ef             = EFlag.Instance;
            EChargeDeploy ecd    = EChargeDeploy.Instance;
            ERounds er           = ERounds.Instance;
        }

        /// <summary>
        /// Initializes all core IRC commands by creating their singleton instance
        /// Only works once (Calls static constructors)
        /// </summary>
        public static void InitializeIrcCommands()
        {
            ICRaw raw                 = ICRaw.Instance;
            ICCommunicate communicate = ICCommunicate.Instance;
            ICServerInfo si           = ICServerInfo.Instance;
            ICBotInfo bi              = ICBotInfo.Instance;
        }

        /// <summary>
        /// Initializes all core Rcon text parsers by creating their singleton instance
        /// Only works once (Calls static constructors)
        /// </summary>
        public static void InitializeRconParsers()
        {
            RStatus status = RStatus.Instance;
        }
#pragma warning restore 0168
        /// <summary>
        /// Called to force the bot to parse a new log line
        /// </summary>
        /// <param name="line">Line to parse</param>
        public void LogHandler(string line)
        {
            try
            {
                line = line.Substring(line.IndexOf(": ") + 2);
                
                if (rFlagEvent.IsMatch(line))
                {
                    //Groups
                    // Player: 1-4
                    // Event: 5
                    Match match = rFlagEvent.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    string evt = match.Groups[5].Value;
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        if (String.Compare(evt, "dropped", true) != 0)
                            // The player can some times drop the flag -after- he has left so we don't want to re-add him here just in case.
                            Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    FlagEvent fEvent;
                    if (String.Compare(evt, "picked up", true) == 0)
                        fEvent = FlagEvent.PICKUP;
                    else if (String.Compare(evt, "dropped", true) == 0)
                        fEvent = FlagEvent.DROP;
                    else if (String.Compare(evt, "defended", true) == 0)
                        fEvent = FlagEvent.DEFEND;
                    else if (String.Compare(evt, "captured", true) == 0)
                        fEvent = FlagEvent.CAPTURE;
                    else
                        fEvent = FlagEvent.UNKNOWN;
                    if (OnFlagEvent != null)
                        OnFlagEvent(player, fEvent);
                }
                else if (line.Equals("server cvars start"))
                    _cvar_list = true;
                else if (line.Equals("server cvars end"))
                    _cvar_list = false;
                else if (_cvar_list)
                    return;
                else if (line.ToLower().StartsWith("server_cvar:"))
                    return;
                else if (rMiniRoundStart.IsMatch(line))
                {
                    if (OnMiniRoundStart != null)
                        OnMiniRoundStart();
                }
                else if (rMiniRoundSelected.IsMatch(line))
                {
                    //Groups
                    //Round: 1
                    if (OnMiniRoundSelected != null)
                        OnMiniRoundSelected(rMiniRoundSelected.Match(line).Groups[1].Value);
                }
                else if (rMiniRoundWin.IsMatch(line))
                {
                    //Groups
                    //Winner: 1
                    //Round: 2
                    Match match = rMiniRoundWin.Match(line);
                    Team winner = Team.GetTeam(match.Groups[1].Value);
                    if (OnMiniRoundWin != null)
                        OnMiniRoundWin(winner, match.Groups[2].Value);
                }
                else if (rMiniRoundLength.IsMatch(line))
                {
                    //Groups
                    //Length: 1 (float, seconds)
                    float seconds = float.Parse(rRoundLength.Match(line).Groups[1].Value);
                    if (OnMiniRoundLength != null)
                        OnMiniRoundLength(new TimeSpan((long)(seconds * 10000000)));
                }
                else if (rRoundStart.IsMatch(line))
                {
                    if (OnRoundStart != null)
                        OnRoundStart();
                }
                else if (rRoundSetup.IsMatch(line))
                {
                    if (OnRoundSetup != null)
                        OnRoundSetup(rRoundSetup.Match(line).Groups[1].Value.Equals("Begin"));
                }
                else if (rRoundWin.IsMatch(line))
                {
                    if (OnRoundWin != null)
                        OnRoundWin(Team.GetTeam(rRoundWin.Match(line).Groups[1].Value));
                }
                else if (rRoundLength.IsMatch(line))
                {
                    float seconds = float.Parse(rRoundLength.Match(line).Groups[1].Value);
                    if (OnRoundLength != null)
                        OnRoundLength(new TimeSpan((long)(seconds * 10000000)));
                }
                else if (rScoreUpdate.IsMatch(line))
                {
                    //Groups
                    //Team: 1
                    //Score: 2
                    //Player Count: 3
                    Match match = rScoreUpdate.Match(line);
                    Team team = Team.GetTeam(match.Groups[1].Value);
                    int score = int.Parse(match.Groups[2].Value);
                    int count = int.Parse(match.Groups[3].Value);

                    if (team.Score != score)
                        team.Score = score;
                    if (Player.GetPlayerList(team).Length != count)
                    {
                        Console.WriteLine(
                                          "Synchronization error detected, player count does not match. Source: Current Score");
                        Console.WriteLine("Refreshing player list...");
                        Player.RefreshList();
                    }

                }
                else if (rMetaPluginLoad.IsMatch(line))
                    return;
                else if (rNameChange.IsMatch(line))
                {
                    //Groups
                    // Player: 1-4
                    // New Name: 5
                    Match match = rNameChange.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    player.Name = match.Groups[4].Value;
                    if (OnNameChange != null)
                        OnNameChange(player, match.Groups[1].Value);
                }
                else if (line.Equals("World triggered \"Game_Over\" reason \"Reached Time Limit\""))
                {
                    if (OnGameTimeout != null)
                        OnGameTimeout();
                }
                else if (line.Equals("World triggered \"Round_Stalemate\""))
                {
                    if (OnRoundStalemate != null)
                        OnRoundStalemate();
                }
                else if (line.Equals("World triggered \"Game_Over\" reason \"Reached Round Limit\""))
                {
                    if (OnGameRoundLimit != null)
                    {
                        OnGameRoundLimit();
                    }
                }
                else if (rSuddenDeath.IsMatch(line))
                {
                    //Groups
                    //Reason 1
                    if (OnSuddenDeath != null)
                        OnSuddenDeath(rSuddenDeath.Match(line).Groups[1].Value);
                }
                else if (rMapLoad.IsMatch(line))
                {
                    //Groups
                    //Map 1
                    if (OnMapLoad != null)
                        OnMapLoad(rMapLoad.Match(line).Groups[1].Value);

                }
                else if (rStartedMap.IsMatch(line))
                {
                    //Groups
                    //Map 1
                    if (OnMapStarted != null)
                        OnMapStarted(rStartedMap.Match(line).Groups[1].Value);

                }
                else if (rRcon.IsMatch(line))
                {
                    Match match = rRcon.Match(line);
                    //Groups
                    //IP:PORT 1
                    //COMMAND 2
                    string ip = match.Groups[1].Value.Substring(0, match.Groups[1].Value.IndexOf(":"));
                    ushort port = ushort.Parse(match.Groups[1].Value.Substring(match.Groups[1].Value.IndexOf(":") + 1));
                    string command = match.Groups[2].Value;
                    if (OnRconReceived != null)
                        OnRconReceived(ip, port, command);
                }
                else if (rChargeDeployed.IsMatch(line))
                {
                    Match match = rChargeDeployed.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;
                    if (OnChargeDeployed != null)
                        OnChargeDeployed(player);
                }

                else if (rSuicide.IsMatch(line))
                {
                    //Groups
                    //Player 1-4
                    //Object 5
                    Match match = rSuicide.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    TF2Object item = TF2Object.GetObject(match.Groups[5].Value);
                    if (OnSuicide != null)
                        OnSuicide(player, item);
                }
                else if (rValidated.IsMatch(line))
                {
                    //Groups
                    //Player 1-3
                    Match match = rValidated.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    if (OnValidated != null)
                        OnValidated(player);
                }
                else if (rDomination.IsMatch(line))
                {
                    //Groups
                    //Player 1-4
                    //Victim 5-8
                    //Assists 9 (OPTIONAL)
                    Match match = rDomination.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    id = int.Parse(match.Groups[6].Value);
                    Player victim = Player.GetPlayer(id);
                    if (victim == null && (victim = Player.GetPlayer(match.Groups[5].Value)) == null)
                    {
                        victim = new Player(id, match.Groups[7].Value, match.Groups[5].Value);
                        Player.AddPlayer(victim);
                    }
                    victim.Team = Team.GetTeam(match.Groups[8].Value);
                    victim.Id = id;
                    victim.SteamID = match.Groups[7].Value;

                    int assists = 0;
                    if (!match.Groups[9].Value.Equals(""))
                        assists = int.Parse(match.Groups[9].Value);

                    if (OnDomination != null)
                        OnDomination(player, victim, assists != 0);
                }
                else if (rRevenge.IsMatch(line))
                {
                    //Groups
                    //Player 1-4
                    //Victim 5-8
                    //Assists 9 (OPTIONAL)
                    Match match = rRevenge.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    id = int.Parse(match.Groups[6].Value);

                    Player victim = Player.GetPlayer(id);
                    if (victim == null && (victim = Player.GetPlayer(match.Groups[5].Value)) == null)
                    {
                        victim = new Player(id, match.Groups[7].Value, match.Groups[5].Value);
                        Player.AddPlayer(victim);
                    }
                    victim.Team = Team.GetTeam(match.Groups[8].Value);
                    victim.Id = id;
                    victim.SteamID = match.Groups[7].Value;

                    int assists = 0;
                    if (!match.Groups[9].Value.Equals(""))
                        assists = int.Parse(match.Groups[9].Value);
                    if (OnRevenge != null)
                        OnRevenge(player, victim, assists != 0);
                }
                else if (rBuiltObject.IsMatch(line))
                {
                    //Groups
                    //Player 1-4
                    //Object 5
                    Match match = rBuiltObject.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    TF2Object item = TF2Object.GetObject(match.Groups[5].Value);
                    if (OnObjectBuilt != null)
                        OnObjectBuilt(player, item);
                }
                else if (rAssist.IsMatch(line))
                {
                    Match match = rAssist.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    id = int.Parse(match.Groups[6].Value);
                    Player victim = Player.GetPlayer(id);
                    if (victim == null && (victim = Player.GetPlayer(match.Groups[5].Value)) == null)
                    {
                        victim = new Player(id, match.Groups[7].Value, match.Groups[5].Value);
                        Player.AddPlayer(victim);
                    }
                    victim.Team = Team.GetTeam(match.Groups[8].Value);
                    victim.Id = id;
                    victim.SteamID = match.Groups[7].Value;
                    if (OnAssist != null)
                        OnAssist(player, victim);
                }
                else if (rEnter.IsMatch(line))
                {
                    Match match = rEnter.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        player.Team = Team.UNKNOWN;
                        Player.AddPlayer(player);
                    }
                    player.Stay = 0.0f;
                    player.Team = Team.LOBBY;
                    if (OnEnter != null)
                        OnEnter(player);
                }
                else if (rSay.IsMatch(line))
                {
                    // Groups
                    // User 1-4
                    // Text 5
                    Match match = rSay.Match(line);
                    int id = int.Parse(match.Groups[2].Value);

                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;
                    if (OnSay != null)
                        OnSay(player, match.Groups[5].Value, false);

                    if (match.Groups[5].Value.StartsWith(Config.CommandPrefix))
                    {
                        if (OnGameCommand != null)
                        {
                            string command, parameters;
                            if (match.Groups[5].Value.Contains(' '))
                            {
                                command = match.Groups[5].Value.Substring(0, match.Groups[5].Value.IndexOf(' '));
                                parameters = match.Groups[5].Value.Substring(match.Groups[5].Value.IndexOf(' ') + 1);
                            }
                            command = match.Groups[5].Value.Substring(1);
                            parameters = "";
                            OnGameCommand(command, parameters, player, false);
                        }
                    }


                }
                else if (rConsoleSay.IsMatch(line))
                {
                    if (OnSay != null)
                        OnSay(Player.CONSOLE, rConsoleSay.Match(line).Groups[1].Value, false);
                }

                else if (rDisconnect.IsMatch(line.Replace("\x0A", "").Trim()))
                {
                    //The steam disconnects like "No steam logon" have a newline (10) for some ungodly
                    //reason before the ") part
                    line = line.Replace("\x0A", "");
                    //Groups
                    //Player 1-4
                    //Reason 5
                    Match match = rDisconnect.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    string substr = line.Substring(line.IndexOf("\")") + 2);

                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        Console.WriteLine("Error, unknown player left, DEBUG {0}", line);
                        return;
                    }
                    Player.DelPlayer(player);
                    if (player != null && OnDisconnect != null)
                        OnDisconnect(player, match.Groups[5].Value);
                }
                else if (rSayTeam.IsMatch(line))
                {
                    // Groups
                    // User 1-4
                    // Text 5
                    Match match = rSayTeam.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;
                    if (OnSay != null)
                        OnSay(player, match.Groups[5].Value, true);

                    if (match.Groups[5].Value.StartsWith(Config.CommandPrefix))
                    {
                        if (OnGameCommand != null)
                        {
                            string command, parameters;
                            if (match.Groups[5].Value.Contains(' '))
                            {
                                command = match.Groups[5].Value.Substring(0, match.Groups[5].Value.IndexOf(' '));
                                parameters = match.Groups[5].Value.Substring(match.Groups[5].Value.IndexOf(' ') + 1);
                            }
                            command = match.Groups[5].Value.Substring(1);
                            parameters = "";
                            OnGameCommand(command, parameters, player, true);
                        }
                    }
                }
                else if (rConnect.IsMatch(line))
                {
                    // Groups
                    // User 1-3
                    // IP 4
                    // Port 5
                    Match match = rConnect.Match(line);
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        player.Team = Team.UNKNOWN;
                        Player.AddPlayer(player);
                    }
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;
                    player.IP = match.Groups[4].Value;
                    System.Net.Dns.BeginGetHostAddresses(player.IP,
                        (result) =>
                        {
                            try
                            {
                                IPAddress[] results;
                                if (result == null || (results = System.Net.Dns.EndGetHostAddresses(result)).Length == 0)
                                    throw new System.Net.Sockets.SocketException();
                                else
                                    player.Host = results[0];
                            }
                            catch (System.Net.Sockets.SocketException)
                            {
                                player.Host = null;
                                IRC.Instance.Broadcast("Unable to resolve {0} host ({1})",
                                    player.PossessiveName,
                                    match.Groups[4].Value);
                            }
                        }
                        ,
                        null);


                    ushort port = ushort.Parse(match.Groups[5].Value);

                    player.Port = port;
                    player.Team = Team.LOBBY;
                    if (OnConnect != null)
                        OnConnect(player);
                }
                else if (rRoleChange.IsMatch(line))
                {
                    Match match = rRoleChange.Match(line);
                    // Groups
                    // Person 1-4
                    // Role 5
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        player.Team = Team.GetTeam(match.Groups[4].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    Role oldRole = player.Role;
                    player.Role = Role.GetRole(match.Groups[5].Value);
                    if (OnRoleChange != null)
                        OnRoleChange(player, oldRole);
                }
                else if (rKillObject.IsMatch(line))
                {
                    Match match = rKillObject.Match(line);

                    // Groups
                    // person 1-4
                    // object 5
                    // weapon 6
                    // object-owner 7-10
                    int id = int.Parse(match.Groups[2].Value);
                    Player player = Player.GetPlayer(id);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(id, match.Groups[3].Value, match.Groups[1].Value);
                        player.Team = Team.GetTeam(match.Groups[4].Value);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = id;
                    player.SteamID = match.Groups[3].Value;

                    TF2Object weapon = TF2Object.GetObject(match.Groups[6].Value);
                    TF2Object item = TF2Object.GetObject(match.Groups[5].Value);
                    id = int.Parse(match.Groups[8].Value);
                    Player owner = Player.GetPlayer(id);
                    if (owner == null && (owner = Player.GetPlayer(match.Groups[7].Value)) == null)
                    {
                        owner = new Player(id, match.Groups[9].Value, match.Groups[7].Value);

                    }
                    owner.Team = Team.GetTeam(match.Groups[10].Value);
                    owner.Id = id;
                    owner.SteamID = match.Groups[9].Value;

                    if (OnObjectKill != null)
                        OnObjectKill(player, owner, item, weapon);

                }
                else if (rTeamChange.IsMatch(line))
                {
                    // Groups
                    // Person 1-4
                    // Team 5
                    int pId = 0;
                    Match m = rTeamChange.Match(line);
                    Player p;
                    p = Player.GetPlayer(pId = int.Parse(m.Groups[2].Value));
                    if (p == null && (p = Player.GetPlayer(m.Groups[1].Value)) == null)
                    {
                        p = new Player(pId, "", "");
                        Player.AddPlayer(p);
                    }
                    p.Name = m.Groups[1].Value;
                    p.SteamID = m.Groups[3].Value;
                    Team oldTeam = p.Team;
                    p.Team = Team.GetTeam(m.Groups[5].Value);
                    p.Id = pId;
                    if (oldTeam == Team.UNKNOWN)
                        oldTeam = Team.GetTeam(m.Groups[4].Value);
                    if (OnTeamChange != null && p.Team != oldTeam)
                        OnTeamChange(p, oldTeam);

                }
                else if (rKill.IsMatch(line))
                {
                    Match match = rKill.Match(line);
                    //Group Info: Killer: 1-4 
                    //Victim: 5-8
                    //TF2Object: 9 (OPTIONAL)
                    //Other: 10
                    int pId = int.Parse(match.Groups[2].Value);
                    string pName = match.Groups[1].Value, pSId = match.Groups[3].Value;

                    Player player = Player.GetPlayer(pId);
                    if (player == null && (player = Player.GetPlayer(match.Groups[1].Value)) == null)
                    {
                        player = new Player(pId, pSId, pName);
                        Player.AddPlayer(player);
                    }
                    player.Team = Team.GetTeam(match.Groups[4].Value);
                    player.Id = pId;
                    player.SteamID = pSId;

                    Player victim = null;

                    pName = match.Groups[5].Value;
                    pId = int.Parse(match.Groups[6].Value);
                    pSId = match.Groups[7].Value;

                    victim = Player.GetPlayer(pId);
                    if (victim == null && (victim = Player.GetPlayer(pName)) == null)
                    {
                        victim = new Player(pId, pSId, pName);
                        Player.AddPlayer(victim);
                    }
                    victim.Team = Team.GetTeam(match.Groups[8].Value);
                    victim.Id = pId;
                    victim.SteamID = pSId;

                    string weapon = match.Groups[9].Value;
                    string other = match.Groups[10].Value;
                    // (customkill "headshot")
                    Regex rCustomKill = new Regex("(customkill \"([^\"]+)\")");
                    string customKill = "";
                    if (rCustomKill.IsMatch(line))
                    {
                        customKill = rCustomKill.Match(line).Groups[1].Value;
                    }

                    if (OnKill != null)
                    {
                        OnKill(player, victim, TF2Object.GetObject(weapon), customKill);
                    }

                }
                else
                {
                    if (Config.CheckEnabled("UnhandledLog"))
                    {
                        IRC.Instance.Broadcast("Unhandled log: {0}", line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception parsing the log line: {0}", e);
                Console.WriteLine("Exception is: {0}", e);
                

                if (Config.CheckEnabled("ShowExceptions"))
                {
                    IRC.Instance.Broadcast("An exception occurred parsing a line: {0}", e.Message);
                    IRC.Instance.Broadcast(e.StackTrace);
                }
                else
                {
                    IRC.Instance.Broadcast("There was an exception parsing a line of log, please report the error in the console and restart your bot.");
                }
            }
        }
    }
}
