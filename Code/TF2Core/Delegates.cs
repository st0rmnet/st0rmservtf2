﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace St0rm.TF2.Core.Delegates {
    /// <summary>
    /// Handles events where one player kills another
    /// </summary>
    /// <param name="killer">Player who killed the victim</param>
    /// <param name="victim">Player whom the killer killed</param>
    /// <param name="weapon">TF2Object used to kill the Victim</param>
    /// <param name="custom">Any other data sent by the server for this log</param>
    public delegate void KillHandler(Player killer, Player victim, TF2Object weapon, string custom);
    /// <summary>
    /// Handles events where a player changes (or first chooses) his team
    /// </summary>
    /// <param name="player">Player which chose a team</param>
    /// <param name="oldTeam">Team the player is switching from</param>
    public delegate void TeamChangeHandler(Player player, Team oldTeam);
    /// <summary>
    /// Handles events where a player kills another player's object
    /// </summary>
    /// <param name="killer">Player which killed owner's object</param>
    /// <param name="owner">Player whom owned the object which killer killed</param>
    /// <param name="item">Object which was killed by killer</param>
    /// <param name="weapon">TF2Object used to kill Object</param>
    public delegate void ObjectKillHandler(Player killer, Player owner, TF2Object item, TF2Object weapon);
    /// <summary>
    /// Handles events where a player changes (or first chooses) his role (class)
    /// </summary>
    /// <param name="player">Player whom chose his Role</param>
    /// <param name="oldRole">player's old Role</param>
    public delegate void RoleChangeHandler(Player player, Role oldRole);
    /// <summary>
    /// Handles events where a player connects to the server (Once, BEFORE he joins a round)
    /// </summary>
    /// <param name="player">Player who connected</param>
    public delegate void ConnectHandler(Player player);
    /// <summary>
    /// Handles events where a player says something in the server
    /// </summary>
    /// <param name="player">The Player who spoke</param>
    /// <param name="text">The message player said</param>
    /// <param name="team">True if this was in team chat, false if it was in public</param>
    public delegate void SayHandler(Player player, string text, bool team);
    /// <summary>
    /// Handles events where a player enters the game (after he connects, and enters a round)
    /// </summary>
    /// <param name="player">Player which entered</param>
    public delegate void EnterHandler(Player player);
    /// <summary>
    /// Handles events where a player's steam id is validated
    /// </summary>
    /// <param name="player">Player whom was validated</param>
    public delegate void ValidatedHandler(Player player);
    /// <summary>
    /// Handles events where a player assists in a kill
    /// </summary>
    /// <param name="player">Player who assisted in a kill</param>
    /// <param name="victim">Player which was killed</param>
    public delegate void AssistHandler(Player player, Player victim);
    /// <summary>
    /// Handles events where a player builds an object
    /// </summary>
    /// <param name="player">Player which built item</param>
    /// <param name="item">TF2Object which was built by player</param>
    public delegate void ObjectBuiltHandler(Player player, TF2Object item);
    /// <summary>
    /// Handles events where a player dominates another
    /// </summary>
    /// <param name="player">Player which dominated victim</param>
    /// <param name="victim">Player which was dominated</param>
    /// <param name="assisted">True if the player only got domination by an assisted kill</param>
    public delegate void DominationHandler(Player player, Player victim, bool assisted);
    /// <summary>
    /// Handles events where a player takes revenge on another which previously dominated him
    /// </summary>
    /// <param name="player">Player who took revenge on victim</param>
    /// <param name="victim">Player which was killed and who previously dominated player</param>
    /// <param name="assisted">True if the player only got revenge by an assisted kill</param>
    public delegate void RevengeHandler(Player player, Player victim, bool assisted);
    /// <summary>
    /// Handles events where a player disconnects from the server
    /// </summary>
    /// <param name="player">Player which was disconnected</param>
    /// <param name="reason">Reason the player disconnected</param>
    public delegate void DisconnectHandler(Player player, string reason);
    /// <summary>
    /// Handles events where a player commits suicide
    /// </summary>
    /// <param name="player">Player which killed himself</param>
    /// <param name="item">TF2Object used to kill himself, can be a weapon represented as a TF2Object</param>
    public delegate void SuicideHandler(Player player, TF2Object item);
    /// <summary>
    /// Handles events where a player deploys charge
    /// </summary>
    /// <param name="player">Player which deployed a charge</param>
    public delegate void ChargeDeployedHandler(Player player);
    /// <summary>
    /// Handles events where a player does something to a "Flag"
    /// Note: Flag is a general name and includes intelligence briefcases etc
    /// </summary>
    /// <param name="player">Player which triggered the event</param>
    /// <param name="evt">The event which player triggered</param>
    public delegate void FlagEventHandler(Player player, FlagEvent evt);
    /// <summary>
    /// Triggers when an RCON is received from a "remote" computer. It can include this bot.
    /// </summary>
    /// <param name="from_ip">IP from which the RCON command was sent</param>
    /// <param name="from_port">Port from which the RCON command was sent</param>
    /// <param name="command">RCON command sent</param>
    public delegate void RconHandler(string from_ip, ushort from_port, string command);
    /// <summary>
    /// Handles events where a player changes his name after he connects
    /// </summary>
    /// <param name="player">Player which changes his name</param>
    /// <param name="old_name">Previous name of player</param>
    public delegate void NameChangeHandler(Player player, string old_name);
    /// <summary>
    /// Handles events where a round starts
    /// </summary>
    public delegate void RoundStartHandler();
    /// <summary>
    /// Handles events where a map begins to load
    /// </summary>
    /// <param name="map_name">Name of the map loading</param>
    public delegate void MapLoadHandler(string map_name);
    /// <summary>
    /// Handles events where a map finishes loading and starts playing
    /// </summary>
    /// <param name="map_name">Name of the map playing</param>
    public delegate void MapStartedHandler(string map_name);
    /// <summary>
    /// Handles events where a round setup begins and ends
    /// </summary>
    /// <param name="started">True of the round setup is starting, False if it is ending</param>
    public delegate void RoundSetupHandler(bool started);
    /// <summary>
    /// Handles events where a round has ended and a winner has been decided
    /// </summary>
    /// <param name="winner">Team which won</param>
    public delegate void RoundWinHandler(Team winner);
    /// <summary>
    /// Handles events where a round has ended and the length of the round is reported
    /// </summary>
    /// <param name="length">TimeSpan in which the round lasted</param>
    public delegate void RoundLengthHandler(TimeSpan length);
    /// <summary>
    /// Handles events where a mini round has started
    /// </summary>
    public delegate void MiniRoundStartHandler();
    /// <summary>
    /// Handles events where a mini round has been selected
    /// </summary>
    /// <param name="round">The name of the round selected, for example: round_1 round_2 round_a</param>
    public delegate void MiniRoundSelectedHandler(string round);
    /// <summary>
    /// Handles events where a mini round has ended and a winner has been decided
    /// </summary>
    /// <param name="winner">Team which won the mini round</param>
    /// <param name="round">Round which was won, for example: round_1 round_2 round_a</param>
    public delegate void MiniRoundWinHandler(Team winner, string round);
    /// <summary>
    /// Handles events where a mini round has ended and the length of the mini round is reported
    /// </summary>
    /// <param name="length">TimeSpan in which the round lasted</param>
    public delegate void MiniRoundLengthHandler(TimeSpan length);

    /// <summary>
    /// Handles events where an irc command has been requested
    /// </summary>
    /// <param name="command">The command without the prefix</param>
    /// <param name="parameters">The parameters of the command</param>
    /// <param name="data">The IRC data object for any additional irc related information about the event</param>
    /// <returns>Whether this command was handled or not</returns>
    public delegate bool IrcComandHandler(string command, string parameters, DPDN.Irc.Client.ChannelMessageReceivedEventArgs data);

    /// <summary>
    /// Handles events where a command has been requested from an ingame player
    /// </summary>
    /// <param name="command">The command without the prefix</param>
    /// <param name="parameters">All parameters of the command</param>
    /// <param name="caller">The Player which called this command</param>
    /// <param name="team">Whether or not this command was done in team chat</param>
    /// <returns>Whether this command was handled or not</returns>
    public delegate bool GameCommandHandler(string command, string parameters, Player caller, bool team);
    /// <summary>
    /// Handle events where a round has ended in a tie
    /// </summary>
    public delegate void RoundStalemateHandler();
    /// <summary>
    /// Handles events where a game has ended due to time running out
    /// </summary>
    public delegate void GameTimeoutHandler();
    /// <summary>
    /// Handles events where a game has ended due to the round limit being reached
    /// </summary>
    public delegate void GameRoundLimitHandler();
    /// <summary>
    /// Handles events where a round has entered sudden death
    /// <param name="reason">The reason for entering sudden death, usually equal to "round timelimit reached"</param>
    /// </summary>
    public delegate void SuddenDeathHandler(string reason);
    /// <summary>
    /// Enumeration of flag events
    /// </summary>
    public enum FlagEvent {
        /// <summary>
        /// The flag was picked up
        /// </summary>
        PICKUP,
        /// <summary>
        /// The flag was dropped
        /// </summary>
        DROP,
        /// <summary>
        /// The flag was defended
        /// </summary>
        DEFEND,
        /// <summary>
        /// The flag was captured
        /// </summary>
        CAPTURE,
        /// <summary>
        /// Some flag event that was not handled was triggered - should theoretically never occur
        /// </summary>
        UNKNOWN
    }
}
