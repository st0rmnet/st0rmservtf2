﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Text;
using DPDN.Irc.Client;
using System.Threading; 
namespace St0rm.TF2.Core {
    /// <summary>
    /// The IRC manager
    /// </summary>
    public class IRC {
        static IrcClient _client;
        /// <summary>
        /// The underlieing Client for direct access
        /// </summary>
        public static DPDN.Irc.Client.IrcClient Client { get { return _client; } }
        /// <summary>
        /// The singleton instance
        /// </summary>
        public static IRC Instance = new IRC();

        /// <summary>
        /// Triggered when an irc command is given
        /// </summary>
        public event Delegates.IrcComandHandler OnIrcCommand;
        

        static IRC() {
            try {
                
                _client = new IrcClient {Encoding = Encoding.Default};

                _client.OnConnect += (sender, args) =>
                                     {
                                         if (Config.OperID != "")
                                         {
                                             _client.Send(String.Format("OPER {0} {1}", Config.OperID, Config.OperPass));
                                         }

                                         System.Xml.XmlNode node = Config.Xml.SelectSingleNode("config//irc//raws");

                                         foreach (string raw in node.InnerText.Replace("\x0D", "").Split((char)10))
                                             if (raw.Trim().Length > 3)
                                                 _client.Send(raw.Trim());


                                         ThreadPool.QueueUserWorkItem(
                                                                      (x) =>
                                                                      {
                                                                          Thread.Sleep(1000);
                                                                          foreach (string c in Config.Channels)
                                                                              _client.Send("JOIN " + c);
                                                                      }
                                             );


                                     };
                _client.Server    = Config.IRCServer;
                _client.Port      = Config.IRCPort;
                _client.Nick      = Config.Nick;
                _client.UserIdent = Config.User;
                _client.Connect();
                _client.OnChannelMessage += (sender, args) => { Instance.CheckCommand(args); };
            }
            catch (Exception e) {
                Console.Write(e);
            }
        }
        /// <summary>
        /// Sends text to all channels in the config file
        /// </summary>
        /// <param name="text">Text to send</param>
        public void Broadcast(string text) {
            foreach (string chan in Config.Channels)
                Client.SendM(chan, text);
        }
        /// <summary>
        /// Sends formatted text to all channels in the config file
        /// </summary>
        /// <param name="format">Formatting of message</param>
        /// <param name="foo">Format parameters</param>
        public void Broadcast(string format, params object[] foo) {
            Broadcast(String.Format(format, foo));
        }

        private void CheckCommand(ChannelMessageReceivedEventArgs data) {
            if (data.Message.ToLower().StartsWith(Config.CommandPrefix.ToLower())) {
                String[] messageArray = data.Message.Split(' ');
                string command = messageArray[0].Substring(Config.CommandPrefix.Length);
                string parameters = "";
                if (messageArray.Length > 1)
                    parameters = data.Message.Substring(data.Message.IndexOf(' ')+1);
                if (OnIrcCommand != null) {
                    ThreadPool.QueueUserWorkItem((x) => OnIrcCommand(command, parameters, data));
                    
                }
            }
        }
        /// <summary>
        /// Gets the IRCAccess for this user for easy comparison
        /// </summary>
        /// <param name="u">User to get the access of</param>
        /// <param name="channel">Channel on which to get the user's access</param>
        /// <returns>IRCAccess of user on channel</returns>
        /// 
        public IRCAccess GetAccess(DPDN.Irc.User u, string channel) {
            
            if (u.IsOwner(channel))
                return IRCAccess.OWNER;
            else if (u.IsAdmin(channel))
                return IRCAccess.ADMIN;
            else if (u.IsOp(channel))
                return IRCAccess.OP;
            else if (u.IsHalfOp(channel))
                return IRCAccess.HALFOP;
            else if (u.IsVoice(channel))
                return IRCAccess.VOICE;
            return IRCAccess.REG;

        }
    }
    /// <summary>
    /// An enumeration representation of user's on IRC and their relative access
    /// </summary>
    public enum IRCAccess {
#pragma warning disable 1591
        REG,
        VOICE,
        HALFOP,
        OP,
        ADMIN,
        OWNER,
        UBER=255
#pragma warning restore 1591
    }
}
