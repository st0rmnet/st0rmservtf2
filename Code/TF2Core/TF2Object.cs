﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Xml;

namespace St0rm.TF2.Core {
    /// <summary>
    /// Represents any object in the game
    /// </summary>
    public class TF2Object {
        static List<TF2Object> objectList = new List<TF2Object>();

        string _name, _display, _article;
        /// <summary>
        /// The TF2 name for this item, not the display name
        /// </summary>
        public string Name { get { return _name; } }
        /// <summary>
        /// The display name of the item as defined on object_list
        /// </summary>
        public string Display { get { return _display; } }
            static TF2Object() {
                BuildList();
        }
        /// <summary>
        /// Gets or sets the article for this item, ie: "an" or "a" based on vowel starting letters.
        /// You can change this to whatever you wish should the need arise
        /// </summary>
        public string Article {
            get {
                    return _article;
            }
            set {
                _article = value;
            }
        }
        private TF2Object(string name, string display) {
            _name = name;
            _display = display;
            if (System.Text.RegularExpressions.Regex.IsMatch(display, "(?i)^[aeiou]"))
                _article = "an";
            else
                _article = "a";
            if (name.Equals("fists"))
                _article = "their";
        }
        /// <summary>
        /// Returns the display name
        /// </summary>
        /// <returns>The display name</returns>
       override public string ToString() {
            return _display;
        }
       /// <summary>
       /// Gets the item with the specified name
       /// </summary>
       /// <param name="name">name of item to get a TF2Object object for</param>
       /// <returns>
       /// <list type="bullet">
       /// <item>TF2Object created from object_list table <b>if</b> it exists</item>
       /// <item>TF2Object created just for this object with display name Unknown (name) <b>if</b> it does not exist in the object_list table</item>
       /// </list>
       /// </returns>
       public static TF2Object GetObject(string name) {
           if (name.Equals(""))
               return new TF2Object("", "No Object");
           lock (objectList) {
               foreach (TF2Object o in objectList) {
                   if (String.Compare(o._name, name, true) == 0)
                       return o;
               }
           }
           return new TF2Object(name,String.Format("Unknown Object ({0})",name)); ;
       }
        /// <summary>
        /// (Re-)Downloads a list of objects from object_list
        /// </summary>
       public static void BuildList() {
           lock (objectList) {
               objectList.Clear();
               MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
               cmd.CommandText = "SELECT * FROM object_list LIMIT 200";
               cmd.Prepare();
               MySqlDataReader reader = cmd.ExecuteReader();
               while (reader.Read()) {
                   TF2Object o = new TF2Object(reader.GetString(0), reader.GetString(1));
                   objectList.Add(o);
               }
               reader.Close();
           }
       }
    }
}
