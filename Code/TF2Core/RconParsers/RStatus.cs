﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

#pragma warning disable 0618

namespace St0rm.TF2.Core.RconParsers {
    /// <summary>
    /// Parses Status replies and can be used to manually update the player list information
    /// </summary>
    class RStatus {
        List<Thread> _waiting = new List<Thread>();
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static RStatus Instance = new RStatus();

        static RStatus() {
            St0rm.TF2.SSQWrapper.SSQ.Instance.OnRconReply += Instance.Handle;

        }

        static System.Text.RegularExpressions.Regex rStatus = new System.Text.RegularExpressions.Regex(@"^# (\d+) ""(.*)"" (STEAM_\S+)\s? (\S+) (\d+) (\d+) (\S+) (\S+)$");

        void Handle(string reply) {
            foreach (string _line in reply.Split(new char[] {'\n'}))  {
                string line = _line.Replace("\r","").Trim();
                if (rStatus.IsMatch(line)) {
                    //# userid name uniqueid connected ping loss state adr
                    //# 924 "fun" STEAM_0:1:2849430 06:09 308 0 active 58.7.113.121:27005
                    //# 925 "Kirby" STEAM_0:0:16705645 04:57 89 0 active 71.194.68.100:10063
                    //Groups
                    //ID: 1
                    //Name: 2
                    //SteamID: 3
                    //TimeConnected: 4
                    //Ping: 5
                    //PacketsLost: 6
                    //State: 7
                    //IP:PORT: 8
                    System.Text.RegularExpressions.Match match = rStatus.Match(line);


                    int id                = int.Parse(match.Groups[1].Value);
                    string name           = match.Groups[2].Value;
                    string steam_id       = match.Groups[3].Value;
                    string time_connected = match.Groups[4].Value;
                    ushort ping           = ushort.Parse(match.Groups[5].Value);
                    int packets_lost      = int.Parse(match.Groups[6].Value);
                    string state          = match.Groups[7].Value;
                    string ip             = match.Groups[8].Value.Substring(0, match.Groups[8].Value.IndexOf(":"));
                    ushort port           = ushort.Parse(match.Groups[8].Value.Substring(match.Groups[8].Value.IndexOf(":") + 1));

                    Player plr = Player.GetPlayer(id);
                    if (plr == null)
                        plr = Player.GetPlayer(name);
                    if (plr == null) {
                        ConsoleColor c = Console.ForegroundColor;
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Unknown player in status list: {0} ID: {1}", name, id);
                        Console.ForegroundColor = c;
                        continue;
                    }

                    plr.Name = name;
                    plr.Id = id;
                    plr.SteamID = steam_id;
                    plr.Host = null;
                    plr.IP = ip;
                    if (plr.Host == null) {

                        System.Net.Dns.BeginGetHostAddresses(plr.IP, (result) => {
                            try {
                                System.Net.IPAddress[] results;
                                if (result == null || (results = System.Net.Dns.EndGetHostAddresses(result)).Length == 0)
                                    throw new System.Net.Sockets.SocketException();
                                else
                                    plr.Host = results[0];
                            }
                            catch (System.Net.Sockets.SocketException) {
                                plr.Host = null;
                                ConsoleColor c = Console.ForegroundColor;
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("Unable to resolve {0} host ({1})", plr.PossessiveName, match.Groups[4].Value);
                                Console.ForegroundColor = c;

                            }
                        }
                  , null);
                    }
                    plr.Port = port;

                }
            }
        }
    }
}
