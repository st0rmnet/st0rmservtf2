﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace St0rm.TF2.Core {
    /// <summary>
    /// Manages player offenses
    /// </summary>
    public class OffenseManager {
        /// <summary>
        /// The singleton instance
        /// </summary>
        public static OffenseManager Instance = new OffenseManager();
        private OffenseManager() { }

        /// <summary>
        /// Adds an offense to user from another Player
        /// </summary>
        /// <param name="offender">The Player who was offended</param>
        /// <param name="vindicator">The player who placed the offense</param>
        /// <param name="type">The type of offense</param>
        /// <param name="reason">The reason for the offense</param>
        /// <returns>Success</returns>
        public bool AddOffense(Player offender, Player vindicator, string type, string reason) {
            DateTime now = DateTime.Now;
            MySql.Data.MySqlClient.MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
            cmd.CommandText = "INSERT INTO offenses (type, name, steamid, ip, time,vname, vsteamid, vip, vpid, vsteamid,vip ,irc,reason) VALUES (@type, @name, @steamid,@ip, @time, @vindicator, @vsteamid, @vip, '0', @reason)";
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@type", type.ToUpper());
            cmd.Parameters.AddWithValue("@name", offender.Name);

            cmd.Parameters.AddWithValue("@steamid", offender.SteamID);
            cmd.Parameters.AddWithValue("@ip", offender.IP);
            cmd.Parameters.AddWithValue("@time", now.Ticks);
            cmd.Parameters.AddWithValue("@vindicator", vindicator.Name);

            cmd.Parameters.AddWithValue("@vsteamid", vindicator.SteamID);
            cmd.Parameters.AddWithValue("@vip", vindicator.IP);
            cmd.Parameters.AddWithValue("@reason", reason);
            return cmd.ExecuteNonQuery() > 0;
          
        }
        /// <summary>
        /// Adds an offense to user from an irc user
        /// </summary>
        /// <param name="offender">The Player who was offended</param>
        /// <param name="vindicator">The user who placed the offense</param>
        /// <param name="type">The type of offense</param>
        /// <param name="reason">The reason for the offense</param>
        /// <param name="channel">The channel the user was in when he created this offense</param>
        /// <returns>Success</returns>
        public bool AddOffense(Player offender, DPDN.Irc.User vindicator, string type, string channel, string reason) {
            DateTime now = DateTime.Now;
            MySql.Data.MySqlClient.MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
            cmd.CommandText = "INSERT INTO offenses (type, name, steamid, ip, time, vname,irc, channel,reason) VALUES (@type, @name, @steamid, @ip, @time, @vindicator, '1', @channel, @reason)";
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@type", type.ToUpper());
            cmd.Parameters.AddWithValue("@name", offender.Name);
            cmd.Parameters.AddWithValue("@steamid", offender.SteamID);
            cmd.Parameters.AddWithValue("@ip", offender.IP);
            cmd.Parameters.AddWithValue("@time", now.Ticks);
            cmd.Parameters.AddWithValue("@vindicator", vindicator.Name);
            cmd.Parameters.AddWithValue("@channel", channel);
            cmd.Parameters.AddWithValue("@reason", reason);
            return cmd.ExecuteNonQuery() > 0;

        }

        /// <summary>
        /// Gets offenses of users which match any of these items. null or 0 items are ignored
        /// </summary>
        /// <param name="name">Name of a user</param>
        /// <param name="steamid">Steam ID of a user</param>
        /// <param name="ip">IP of a user</param>
        /// <returns>All matching offenses</returns>
        public Offense[] GetOffenses(string name, string steamid, string ip) {
            List<Offense> offenses = new List<Offense>();
            if (name != null) {
                MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM offenses WHERE name=@name";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", name);
                MySqlDataReader reader = cmd.ExecuteReader();
                Offense[] from_db = RetrieveStructs(reader);
                offenses = offenses.Union(from_db.AsEnumerable()).ToList();
                reader.Close();
            }
            if (steamid != null) {
              
                MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM offenses WHERE steamid=@sid";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@sid", name);
                MySqlDataReader reader = cmd.ExecuteReader();
                Offense[] from_db = RetrieveStructs(reader);
                offenses = offenses.Union(from_db.AsEnumerable()).ToList();
                reader.Close();
        }
            if (ip != null) {
               
                MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM offenses WHERE ip=@ip";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@ip", name);
                MySqlDataReader reader = cmd.ExecuteReader();
                Offense[] from_db = RetrieveStructs(reader);
                offenses = offenses.Union(from_db.AsEnumerable()).ToList();
                reader.Close();
         
            }
            return offenses.ToArray();

        }
        /// <summary>
        /// Retrieve an Offense[] built from an `offense` table reader
        /// WARNING: Does -not- close the reader
        /// </summary>
        /// <param name="reader">MySqlDataReader from an `offense` table</param>
        /// <returns></returns>
        public Offense[] RetrieveStructs(MySqlDataReader reader) {
            List<Offense> offenses = new List<Offense>();

            while (reader.Read()) {
                Offense o = new Offense();
                o.fromirc = reader.GetBoolean("irc");
                o.name = reader.GetString("name");
                o.steamid = reader.GetString("steamid");
                o.ip = reader.GetString("ip");
                o.time = new DateTime(reader.GetInt64("time"));
                o.vindicator = reader.GetString("vname");
                o.type = reader.GetString("type");
                if (o.fromirc) {
                    o.channel = reader.GetString("channel");
                }
                else {
                    o.vsteamid = reader.GetString("vsteamid");
                    o.vip = reader.GetString("vip");
                }
                offenses.Add(o);
            }

            return offenses.ToArray();
        }
    }
    /// <summary>
    /// Represents an offense against a user
    /// </summary>
    public struct Offense {
        /// <summary>
        /// The player who comitted this offense
        /// </summary>
        public string name;

        /// <summary>
        /// The SteamID of the player who comitted this offense
        /// </summary>
        public string steamid;
        /// <summary>
        /// The IP of the player who comitted this offense
        /// </summary>
        public string ip;

        /// <summary>
        /// The player OR irc user who added this offense
        /// </summary>
        public string vindicator;
        /// <summary>
        /// Whether or this was added via an irc command
        /// </summary>
        public bool fromirc;
        /// <summary>
        /// IRC channel offense was added on
        /// </summary>
        public string channel;
        /// <summary>
        /// vindicator's steam id
        /// </summary>
        public string vsteamid;
        /// <summary>
        /// vindicator's IP
        /// </summary>
        public string vip;

        /// <summary>
        /// Time offense was added
        /// </summary>
        public DateTime time;
        /// <summary>
        /// Reason for offense
        /// </summary>
        public string reason;

        /// <summary>
        /// Type of offense against user
        /// </summary>
        public string type;
    }
}
