﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using System.Xml;

namespace St0rm.TF2.Core {
    /// <summary>
    /// Represents this bots configuration
    /// Core cached items are static properties
    /// All other items in the configuration file can be accessed via <seealso cref="P:St0rm.TF2.Core.Config.Xml">Xml</seealso>
    /// </summary>
    public class Config {
        /// <summary>
        /// The configuration file's name
        /// </summary>
        public const string CONFIG_FILE = "tf2_st0rmserv.conf";
        /// <summary>
        /// Singleton instance of Config
        /// </summary>
        public static Config Instance = new Config();
        string _nick = "TF2Serv", _user = "TF2", _server = "irc.St0rm.net";
        string _dbhost = "www.st0rm.net", _dbuser = "trivia2", _dbpass = "maow", _dbname = "trivia";
        string _tf2server = "", _logserver = "", _rconpass;
        ushort _tf2port = 0, _logport = 0;
        ushort _port = 6667;
        string _command_prefix = "!";
        string[] _channels = { "" };
        string _oper = null, _operpass = null;

        private XmlDocument _doc = new XmlDocument();
        
        /// <summary>
        /// The XmlDocument for the bot's config file
        /// </summary>
        public static XmlDocument Xml {
            get { return Instance._doc; }
        }

        /// <summary>
        /// The port of the IRC server
        /// </summary>
        public static ushort IRCPort {
            get { return Instance._port; }
            set { Instance._port = value; }
        }
        /// <summary>
        /// The host of the irc server
        /// </summary>
        public static string IRCServer {
            get { return Instance._server; }
            set { Instance._server = value; }
        }
        /// <summary>
        /// The bot's nickname (Use IRC.Client to get the actual one)
        /// </summary>
        public static string Nick {
            get { return Instance._nick; }
            set { Instance._nick = value; }
        }
        /// <summary>
        /// The bot's user (Use IRC.Client to get the actual one)
        /// </summary>
        public static string User {
            get { return Instance._user; }
            set { Instance._user = value; }
        }
        /// <summary>
        /// Channels the bot is set to join (Use IRC.Client to get the actual one)
        /// </summary>
        public static string[] Channels {
            get { return Instance._channels; }
        }
        /// <summary>
        /// The Database name
        /// </summary>
        public static string DBName {
            get { return Instance._dbname; }
        }
        /// <summary>
        /// The Database user
        /// </summary>
        public static string DBUser {
            get { return Instance._dbuser; }
        }
        /// <summary>
        /// The database host
        /// </summary>
        public static string DBHost {
            get { return Instance._dbhost; }
        }
        /// <summary>
        /// The database password
        /// </summary>
        public static string DBPass {
            get { return Instance._dbpass; }
        }
        /// <summary>
        /// The gameserver host
        /// </summary>
        public static string TF2Host {
            get { return Instance._tf2server; }
        }
        /// <summary>
        /// The rcon port
        /// </summary>
        public static ushort TF2Port {
            get { return Instance._tf2port; }
        }
        /// <summary>
        /// The host set to enable logging for (Usually the bots external IP)
        /// </summary>
        public static string LogHost {
            get { return Instance._logserver; }
        }
        /// <summary>
        /// The port set to enable logging for
        /// </summary>
        public static ushort LogPort {
            get { return Instance._logport; }
        }
        /// <summary>
        /// The TF2 Rcon password
        /// </summary>
        public static string RconPass {
            get { return Instance._rconpass; }
        }
        /// <summary>
        /// The oper ID used to oper this bot
        /// </summary>
        public static String OperID {
            get { return Instance._oper; }
        }
        /// <summary>
        /// The oper password used to oper this bot
        /// </summary>
        public static String OperPass {
            get { return Instance._operpass; }
        }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowKills { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowSuicides { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowAssists { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowCharges { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowFlags { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowTeamChat { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowObjectBuilds { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowObjectKills { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowRoleChanges { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowRevenges { get; set; }
        /// <summary>
        /// If this module is enabled
        /// </summary>
        public static bool ShowDominations { get; set; }
        /// <summary>
        /// The prefix of commands to recognize
        /// </summary>
        public static string CommandPrefix { get { return Instance._command_prefix; } set { Instance._command_prefix = value; } }

        static Config() {
            Load();
        }
        /// <summary>
        /// Loads the bots configuration from the XML config file and the database listings
        /// </summary>
        public static void Load() {
         
            if (!File.Exists(CONFIG_FILE)) {
                Console.WriteLine("No configuration file...");
                Environment.Exit(1);
            }
            Xml.Load(CONFIG_FILE);

            #region IRC
            Instance._server = Xml.SelectSingleNode("config//irc//server").InnerText;
            if (!ushort.TryParse(Xml.SelectSingleNode("config//irc//port").InnerText, out Instance._port))
                Die("Bad IRC Port, try 6667");

            Instance._nick = Xml.SelectSingleNode("config//irc//nick").InnerText;
            Instance._user = Xml.SelectSingleNode("config//irc//user").InnerText;
            Instance._channels = Xml.SelectSingleNode("config//irc//channels").InnerText.Replace("\x0D","").Split((char)10);
            // CHANNELS
            if (Xml.SelectSingleNode("config//irc//oper//id") != null && Xml.SelectSingleNode("config//irc//oper//pass") != null) {
                Instance._oper = Xml.SelectSingleNode("config//irc//oper//id").InnerText;
                Instance._operpass = Xml.SelectSingleNode("config//irc//oper//pass").InnerText;
            }
            #endregion

            #region Database
            Instance._dbhost = Xml.SelectSingleNode("config//mysql//host").InnerText;
            Instance._dbuser = Xml.SelectSingleNode("config//mysql//user").InnerText;
            Instance._dbpass = Xml.SelectSingleNode("config//mysql//pass").InnerText;
            Instance._dbname = Xml.SelectSingleNode("config//mysql//name").InnerText;
            #endregion

            #region Game Server
            Instance._tf2server = Xml.SelectSingleNode("config//tf2//host").InnerText;
            if (!ushort.TryParse(Xml.SelectSingleNode("config//tf2//rcon//port").InnerText, out Instance._tf2port))
                Die("Bad RCON port - Consult your server configuration file");
            Instance._rconpass = Xml.SelectSingleNode("config//tf2//rcon//pass").InnerText;
            Instance._logserver = Xml.SelectSingleNode("config//tf2//logs//host").InnerText;
            if (!ushort.TryParse(Xml.SelectSingleNode("config//tf2//logs//port").InnerText, out Instance._logport))
                Die("Bad log server port - chose any open port");

            #endregion

            Instance._command_prefix = Xml.SelectSingleNode("config//commands//prefix").InnerText;

            LoadModules();

            TF2Object.BuildList();
            TF2Object.BuildList();
            Role.BuildList();
        }
            
        /// <summary>
        /// Loads the module statuses from the database (Called automatically via Load())
        /// </summary>
        public static void LoadModules() {
            try {
                ShowKills = CheckEnabled("Kills");
                ShowCharges = CheckEnabled("Charges");
                ShowSuicides = CheckEnabled("Suicides");
                ShowAssists = CheckEnabled("Assists");
                ShowFlags = CheckEnabled("Flags");
                ShowObjectKills = CheckEnabled("ObjectKills");
                ShowObjectBuilds = CheckEnabled("ObjectBuilds");
                ShowTeamChat = CheckEnabled("TeamChat");
                ShowRoleChanges = CheckEnabled("RoleChanges");
                ShowRevenges = CheckEnabled("Revenges");
                ShowDominations = CheckEnabled("Dominations");
            }
            catch (Exception e) {
                Console.WriteLine(e);
            }
        }
        /// <summary>
        /// Returns whether or not a module is enabled in the database 
        /// If it is not in the database, it is added and set to enabled
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns>Enabled status</returns>
        public static bool CheckEnabled(string item) {

            MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
            cmd.CommandText = "SELECT enabled FROM modules WHERE name=@name LIMIT 1";
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@name",item);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read()) {
                bool success = reader.GetInt16("enabled") == 1;
                reader.Close();
                return success;
            }
            else {
                reader.Close();
                cmd.CommandText = "INSERT INTO modules (name,enabled) VALUES (@name,@enabled)";
                cmd.Parameters.Clear();
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@name", item);
                cmd.Parameters.AddWithValue("@enabled", 1);
                bool success = cmd.ExecuteNonQuery() > 0;
                reader.Close();
                return success;
            }
        }
        /// <summary>
        /// Toggles a module on or off in the database
        /// </summary>
        /// <param name="item">Module to toggle</param>
        /// <returns>Success</returns>
        public static bool Toggle(string item) {
        //UPDATE modules SET `enabled`=IF(`enabled`='1','0','1') WHERE name=?
            MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
            //cmd.CommandText = "UPDATE modules SET `enabled`=IF(`enabled`='1','0','1') WHERE name=@name";
            cmd.CommandText = "INSERT INTO modules (name,enabled) VALUES(@name,'1') ON DUPLICATE KEY UPDATE `enabled`=IF(`enabled`=1,'0','1')";
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@name", item);
            bool success = cmd.ExecuteNonQuery() > 0;
            if (success)
            {
                LoadModules();
            }
            return success;
        }

       
        static void Die(string reason)
        {
            Console.WriteLine("Config error, cannot continue: {0}", reason);
            Console.ReadKey();
            Environment.Exit(1);
        }
    }
}
