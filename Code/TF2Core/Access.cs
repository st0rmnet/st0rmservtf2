﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

#pragma warning disable 0659
#pragma warning disable 0661

namespace St0rm.TF2.Core {
    /// <summary>
    /// Represents this player's access as defined in the database
    /// </summary>
    public class PlayerAccess {
        AccessType _type = AccessType.PLAYER;
        List<char> _flags = new List<char>();
        /// <summary>
        /// The default access for a player
        /// </summary>
        public static readonly PlayerAccess DEFAULT = new PlayerAccess(AccessType.PLAYER);

        /// <summary>
        /// Gets the AccessType of access this is
        /// </summary>
        public AccessType Type { get { return _type; } }
        /// <summary>
        /// Gets the List containing the flags for this access level
        /// </summary>
        public List<char> Flags { get { return _flags; } }
        #region Operators
        /// <summary>
        /// Compares two access levels
        /// </summary>
        public static bool operator <(PlayerAccess a1, PlayerAccess a2) {
            return a1.Type > a2.Type;
        }
        /// <summary>
        /// Compares two access levels
        /// </summary>
        public static bool operator >(PlayerAccess a1, PlayerAccess a2) {
            
            return a1.Type < a2.Type;
        }
        /// <summary>
        /// Compares two access levels
        /// </summary>
        public static bool operator ==(PlayerAccess a1, PlayerAccess a2) {
            if ((object)a1 == null && (object)a2 == null)
                return true;
            return a1.Equals(a2);
        }
        /// <summary>
        /// Compares two access levels
        /// </summary>
        public static bool operator !=(PlayerAccess a1, PlayerAccess a2) {
            if ((object)a1 == null && (object)a2 != null || (object)a1 != null && (object)a2 == null)
                return true;
            else if (a1 == null && a2 == null)
                return false;
            return !a1.Equals(a2);
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Gets access of Player <b>from the database</b>
        /// Use P:Player.Access to get the cached version
        /// </summary>
        /// <param name="p">The Player to get the Access of</param>
        /// <returns>Access</returns>
        public static PlayerAccess GetAccess(Player p) {
            PlayerAccess a = null;
            if (p.SteamID != null && p.SteamID != "" && (a = GetAccess(p.SteamID)) != null)
                return a;
            else
                return DEFAULT;
        }
        /// <summary>
        /// Gets the access of a Steam ID <b>from the database</b>
        /// Use P:Player.Access to get the cached version
        /// </summary>
        /// <param name="steam_id">Steam ID to get access of</param>
        /// <returns>Access from db or null if none is given</returns>
        public static PlayerAccess GetAccess(string steam_id) {
            MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
            cmd.CommandText = "SELECT access_type, access_flags FROM players WHERE steam_id=@id";
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@id", steam_id);
            MySqlDataReader reader = cmd.ExecuteReader();
            PlayerAccess a = null;
            if (reader.Read()) {
                AccessType type;
                switch (reader.GetInt16("access_type")) {
                    case -1:
                        type = AccessType.BANNED;
                        break;
                    default:
                        type = (AccessType)reader.GetInt32("access_type");
                        break;
                }
                a = new PlayerAccess(type, reader.GetString("access_flags").ToCharArray());
            }
            reader.Close();
            return a;
        }
        #endregion

        /// <summary>
        /// Compares to access levels for equality
        /// </summary>
        /// <param name="obj">Access level to compare to</param>
        /// <returns></returns>
        public override bool Equals(object obj) {
            if (obj is PlayerAccess) {
                PlayerAccess a1 = (PlayerAccess)obj;
                return a1._type == _type && a1._flags.Equals(_flags);
            }
            return false;
        }
        /// <summary>
        /// Constructs a new Access of type with no flags
        /// </summary>
        /// <param name="type">Type of Access</param>
        public PlayerAccess(AccessType type) : this(type,new char[] { }) {
        }
        /// <summary>
        /// Constructs a new Access of type with the given flags
        /// </summary>
        /// <param name="type">Type of Access</param>
        /// <param name="flags">Default flags</param>
        public PlayerAccess(AccessType type, char[] flags) {
            _type = type;
            foreach (char flag in flags)
                _flags.Add(flag);
        }

    }
    /// <summary>
    /// The Type of Access
    /// </summary>
    public enum AccessType {
#pragma warning disable 1591
        BANNED = -1,
        PLAYER = 0,
        TEMP = 1,
        MODERATOR=10,
        ADMINISTRATOR=100
#pragma warning restore 1591
    }
}
