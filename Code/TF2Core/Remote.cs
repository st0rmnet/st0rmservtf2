﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.SSQWrapper;

namespace St0rm.TF2 {
    /// <summary>
    /// Convience class for using remote commands such as Rcon and Rcon-Say
    /// </summary>
    public class Remote {
        /// <summary>
        /// Sends a raw command to the server via RCON
        /// <b>Warning: Some old invalid rcon commands can crash SSQ.dll</b>
        /// </summary>
        /// <param name="command">Command to send</param>
        public static bool Raw(string command) {
            return SSQ.Instance.Rcon(command); 
        }
        /// <summary>
        /// Sends a formatted raw command to the server via RCON
        /// <b>Warning: Some old invalid rcon commands can crash SSQ.dll</b>
        /// </summary>
        /// <param name="format">Format of command to send</param>
        /// <param name="p">Format parameters</param>
        public static void Raw(string format, params object[] p) {
            Raw (String.Format(format,p));
        }
        /// <summary>
        /// Says something to the server via rcon say
        /// </summary>
        /// <param name="message">String to say</param>
        public static void Say(string message) {
            SSQ.Instance.HideNextRcon();
            Raw("say {0}",message);
        }
        /// <summary>
        /// Says a formatted string to the server via rcon say
        /// </summary>
        /// <param name="format">Format of string to say</param>
        /// <param name="p">Formatting parameters</param>
        public static void Say(string format, params object[] p) {
            Say(String.Format(format, p));
        }
        /// <summary>
        /// Says a string in the server via rcon say using the bots name
        /// </summary>
        /// <param name="message">Message to say</param>
        public static void BotMessage(string message) {
            Say("(St0rmServ): {0}",message);
        }
        /// <summary>
        /// Says a formatted string to the server via ron say using the bots name as the sender
        /// </summary>
        /// <param name="format">Format of string to say</param>
        /// <param name="p">Formatting parameters</param>
        public static void BotMessage(string format, params object[] p) {
            Say(String.Format(format, p));
        }

    }
}
