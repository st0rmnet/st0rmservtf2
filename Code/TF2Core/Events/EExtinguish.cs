﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.Core;

namespace St0rm.TF2.Events {
    class EExtinguish {
        public static EExtinguish Instance = new EExtinguish();
        private EExtinguish() {
            TF2EventHandler.Instance.OnExtinguish += ExtinguishHandler;
        }
        void ExtinguishHandler(Player player, Player target, TF2Object item) {
            if (Config.ShowExtinguishes)
                IRC.Instance.Broadcast("[12Extinguish] {0} extinguished {1} with {2} {3}",player.ColoredName,target.ColoredName,item.Article,item);
        }
    }
}
