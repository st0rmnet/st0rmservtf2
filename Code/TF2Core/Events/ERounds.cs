﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.Core;

namespace St0rm.TF2.Events {
    class ERounds {
        static ERounds _instance = new ERounds();
        public static ERounds Instance { get { return _instance; } }
        private ERounds() {
            TF2EventHandler.Instance.OnRoundStart += RoundStart;
            TF2EventHandler.Instance.OnMapLoad += MapLoading;
            TF2EventHandler.Instance.OnMapStarted += MapStarted;
            TF2EventHandler.Instance.OnRoundWin += RoundWin;
            TF2EventHandler.Instance.OnRoundLength += RoundLength;
            TF2EventHandler.Instance.OnRoundStalemate += RoundTie;
            TF2EventHandler.Instance.OnGameTimeout += GameTimeout;
            TF2EventHandler.Instance.OnGameRoundLimit += GameRoundLimit;
            TF2EventHandler.Instance.OnSuddenDeath += SuddenDeath;
        }
        void RoundStart() {
            IRC.Instance.Broadcast("03Round start!");
            Team.ResetAll();
        }
        void MapLoading(string map) {
            IRC.Instance.Broadcast("03Now loading {0}...",map);
        }
        void MapStarted(string map) {
            IRC.Instance.Broadcast("03{0} loaded!", map);
            SSQWrapper.SSQ.Instance.SetGameServer(Config.TF2Host, Config.TF2Port);
        }
        void RoundWin(Team winner) {
            IRC.Instance.Broadcast("03{0} wins the round.",winner);
        }
        void RoundLength(TimeSpan length) {
            IRC.Instance.Broadcast("03Round lasted {0}", length);
        }
        void RoundTie() {
            IRC.Instance.Broadcast("03Round tied! No team was declared the victor. Better luck next time.");
        }
        void GameTimeout() {
            IRC.Instance.Broadcast("03The clock ran out. How disappointing.");
        }
        void GameRoundLimit() {
            IRC.Instance.Broadcast("03The game has ended - round limit reached.");
        }
        void SuddenDeath(string reason) {
            IRC.Instance.Broadcast("03Sudden death! ({0})",reason);
        }
    }
}

