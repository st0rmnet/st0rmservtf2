﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.Core;
using St0rm.TF2.Core.Delegates;

namespace St0rm.TF2.Events {
    class EFlag {
        static EFlag _instance = new EFlag();
        public static EFlag Instance { get { return _instance; } }
        private EFlag() {
            TF2EventHandler.Instance.OnFlagEvent += Flag;
        }
        void Flag(Player player, FlagEvent evt) {
            if (Config.ShowFlags) {
                switch (evt) {
                    case FlagEvent.CAPTURE:
                        IRC.Instance.Broadcast("[13Flag]: {0}", player.Team.GetColor( String.Format("{0} has captured the flag!", player)));
                        break;
                    case FlagEvent.DEFEND:
                        IRC.Instance.Broadcast("[13Flag]: {0}", player.Team.GetColor( String.Format("{0} got the flag back (defend)!", player)));
                        break;
                    case FlagEvent.DROP:
                        IRC.Instance.Broadcast("[13Flag]: {0}", player.Team.GetColor( String.Format("{0} has dropped the flag :(", player)));
                        break;
                    case FlagEvent.PICKUP:
                        IRC.Instance.Broadcast("[13Flag]: {0}", player.Team.GetColor( String.Format("{0} recovered the flag!", player)));
                        break;
                    case FlagEvent.UNKNOWN:
                        IRC.Instance.Broadcast("[13Flag]: {0}", player.Team.GetColor( String.Format("{0} caused an unknown flag event...", player)));
                        break;
                }
            }
        }
    }
}
