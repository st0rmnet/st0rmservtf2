﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.Core;

namespace St0rm.TF2.Events {
    class ETeamChange {
        static ETeamChange _instance = new ETeamChange();
        public static ETeamChange Instance { get { return _instance; } }
        private ETeamChange() {
            TF2EventHandler.Instance.OnTeamChange += Change;
        }
        void Change(Player player, Team oldTeam) {
            if (oldTeam != Team.LOBBY)
                IRC.Instance.Broadcast("[6Team Selection]: {0}", player.Team.GetColor(String.Format("{0} has left {1} for the {2} team", player, oldTeam, player.Team)));
            else
                IRC.Instance.Broadcast("[6Team Selection]: {0}", player.Team.GetColor(String.Format("{0} has chosen to join the {1} team", player, player.Team)));
        }

    }
}
