﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.Core;

namespace St0rm.TF2.Events {
    class ERoleChange {
        static ERoleChange _instance = new ERoleChange();
        public static ERoleChange Instance { get { return _instance; } }
        private ERoleChange() {
            TF2EventHandler.Instance.OnRoleChange += Change;
        }
        void Change(Player player, Role oldRole) {
            if (Config.ShowRoleChanges)
                IRC.Instance.Broadcast("[06Role]: {0} is now {1} {2} (Old role: {3})", player.ColoredName,player.Role.Article, player.Role, oldRole);
        }

    }
}

