﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.Core;

namespace St0rm.TF2.Events {
    class EAssistSuicide {
        public static EAssistSuicide Instance = new EAssistSuicide();
        private EAssistSuicide() {
            TF2EventHandler.Instance.OnSuicide += Suicide;
            TF2EventHandler.Instance.OnAssist += Assist;
        }
        void Suicide(Player player, TF2Object item) {
            if (Config.ShowSuicides)
                IRC.Instance.Broadcast("[6Suicide]: {0} killed himself using {1} {2}", player.ColoredName,item.Article, item);
        }
        void Assist(Player player, Player victim) {
            if (Config.ShowAssists)
                IRC.Instance.Broadcast("[04Assist]: {0} assisted in killing {1}", player.ColoredName, victim.ColoredName);
        }
    }
}
