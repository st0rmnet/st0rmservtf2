﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.Core;
using St0rm.TF2.Core.Delegates;

namespace St0rm.TF2.Events {
    class ECaptures {
        public static ECaptures Instance = new ECaptures();
        private ECaptures() {
            TF2EventHandler.Instance.OnCaptureBlocked += CaptureBlocked;
            TF2EventHandler.Instance.OnPointCaptured += PointCaptured;
        }
        public void CaptureBlocked(Player player, int cpId, string cpName) {
            IRC.Instance.Broadcast("[13CP]: Capture of Point {0} ({1}) blocked by {2}",cpId,cpName,player.ColoredName);
        }
        public void PointCaptured(Player[] players, int cpId, string cpName) {
            StringBuilder playerList = new StringBuilder();
            for (int i = 0; i < players.Length; i++) {
                playerList.Append(players[i].ColoredName);
                if (i + 1 != players.Length)
                    playerList.Append(", ");
            }

            IRC.Instance.Broadcast("[13CP]: Point {0} ({1}) captured by {2}",cpId,cpName,playerList.ToString());
        }
    }
}
