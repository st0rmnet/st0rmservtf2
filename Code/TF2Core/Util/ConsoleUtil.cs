﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace St0rm.TF2.Core.Util {
    /// <summary>
    /// Used to add methods to Console for reporting
    /// </summary>
    public static class ConsoleUtil {
        /// <summary>
        /// Writes aw warning to the console
        /// </summary>
        /// <param name="warning">Warning to write</param>
        public static void WriteWarning(string warning) {
            ConsoleColor c = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(warning);
            Console.ForegroundColor = c;
        }
        /// <summary>
        /// Writes an error to the console
        /// </summary>
        /// <param name="error">Error to write</param>
        public static void WriteError(string error) {
            ConsoleColor c = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
            Console.ForegroundColor = c;
        }
    }
}
