﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using DPDN.Irc.Client;
namespace St0rm.TF2.Core {
    /// <summary>
    /// Contains information about a command's target
    /// </summary>
    public struct TargetInfo {
        /// <summary>
        /// The ingame player targeted by this command
        /// </summary>
        public Player target;
        /// <summary>
        /// Everything after the target in the command
        /// </summary>
        public string other;
    }
    /// <summary>
    /// Used to get target information from a command string's parameters
    /// </summary>
    public static class TargetUtil {
        /// <summary>
        /// Gets the target information from a command's parameters
        /// </summary>
        /// <param name="cmd_parameters">The parameters of some command to parse</param>
        /// <returns>Target information for these parameters</returns>
        /// <exception cref="T:NoMatchesFoundException">Thrown when there are no single matches</exception>
        public static TargetInfo ParseTarget(this string cmd_parameters) {
            TargetInfo info = new TargetInfo();

            if (System.Text.RegularExpressions.Regex.IsMatch(cmd_parameters, @"^\d+( .*)?")) {
                string[] p_array = cmd_parameters.Split(' ');
                int num = int.Parse(p_array[0]);
                Player p = null;
                if ((p = Player.GetPlayer(p_array[0])) == null)
                    p = Player.GetPlayer(num);
                if (p != null) {
                    info.target = p;
                    if (cmd_parameters.Split(' ').Length > 1)
                        info.other = cmd_parameters.Substring(cmd_parameters.IndexOf(' ') + 1);
                    else
                        info.other = null;
                    return info;
                }

            }

            for (int last_index = -1;;) {
                int end;
                if (last_index + 1 <= cmd_parameters.Length)
                   end = cmd_parameters.IndexOf(' ', last_index + 1);
                else
                    end = cmd_parameters.Length;
      
                if (end == -1)
                    end = cmd_parameters.Length;
                if (end == 0)
                    break;

                string name = cmd_parameters.Substring(0,end);

                if (Player.GetPlayer(name) != null) {
                    info.target = Player.GetPlayer(name);
                    last_index = end;
                    continue;
                }

                Player[] players = Player.FindPlayers(name);
                if (players.Length > 1) {
                    last_index = end;
                    continue;
                }
                else if (players.Length == 0) {
                    if (info.target != null)
                        if (last_index + 1 < cmd_parameters.Length)
                            info.other = cmd_parameters.Substring(last_index + 1);
                     break;
                }
                else if (players.Length == 1)
                    info.target = players[0];
                if (end == cmd_parameters.Length)
                    break;
                if (info.target == null)
                    info.other = cmd_parameters;
                last_index = end;
            }
            if (info.target == null)
                throw new NoMatchesException();
            return info;

        }
    }
    /// <summary>
    /// Thrown when no single match is found.
    /// </summary>
    public class NoMatchesException : System.Exception { }
}
