﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DPDN.Irc.Client;

namespace St0rm.TF2.Core.Util {
    /// <summary>
    /// Used to add methods to IRC related items from DPDN's library
    /// </summary>
    public static class IRCExtensions {
        /// <summary>
        /// Sends a message to the channel speciified in this data
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="message">Message to reply with</param>
        public static void Reply(this DPDN.Irc.Client.ChannelMessageReceivedEventArgs data, string message) {
            IRC.Client.SendM(data.Channel.Name, message);
        }
        /// <summary>
        /// Sends a message to the channel speciified in this data in a format
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="message">Message to reply with</param>
        /// <param name="format">Format data for message</param>
        public static void Reply(this DPDN.Irc.Client.ChannelMessageReceivedEventArgs data, string message, params object[] format) {
            IRC.Client.SendM(data.Channel.Name, message,format);
        }
    }
}
