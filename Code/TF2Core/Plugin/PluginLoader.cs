﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using St0rm.TF2.Core;
using St0rm.TF2.Core.Plugin;

namespace St0rm.TF2.Core.Plugin {
    /// <summary>
    /// Loads and manages plugins
    /// </summary>
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    public class PluginLoader {
        List<IPlugin> _plugins = new List<IPlugin>();
        static PluginLoader _instance = new PluginLoader();
        /// <summary>
        /// The singleton instance
        /// </summary>
        public static PluginLoader Instance { get { return _instance; } }
        /// <summary>
        /// Loads a plugin by file path
        /// </summary>
        /// <param name="file">Path to plugin</param>
        public void Load(string file) {
            if (!File.Exists(file) || !file.EndsWith(".dll",true,null))
                return;
            AppDomain.CurrentDomain.AppendPrivatePath(Path.GetFullPath(file));
            Assembly asm = null;
            
            AppDomainSetup info = new AppDomainSetup();

            info.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;
            info.PrivateBinPath = Path.GetFullPath(file);
            info.DisallowApplicationBaseProbing = false;
            System.Security.Policy.Evidence evidence = new System.Security.Policy.Evidence();
            AppDomain domain = AppDomain.CreateDomain(Path.GetFileName(file));
            
            try {
                Console.WriteLine("Loading {0}...",Path.GetFileName(file));
                AssemblyName name = AssemblyName.GetAssemblyName(file);
                name.CodeBase = Path.GetFullPath(file);
               
               // asm = domain.Load(name);
                asm = Assembly.LoadFile(file);
            }
            catch (Exception e) {
                Console.WriteLine("DLL is not a .NET assembly file {0}",e);
                return;
            }
            if (asm == null) {
                Console.WriteLine("Error loading plugin, assembly is null for {0}", asm);
            }
            
            Type t_iplugin = null;
            try {
                Type[] types = asm.GetTypes();
                Assembly tf2core = AppDomain.CurrentDomain.GetAssemblies().Single(x =>x.GetName().Name.Equals("TF2Core"));
                Type tp = tf2core.GetType("St0rm.TF2.Core.Plugin.IPlugin");
                foreach (Type t in types) {
        
                    if (tp.IsAssignableFrom(t)) {
                        t_iplugin = t;
                        break;
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine("Exception loading type: {0}",e);
                return;
            }
            if (t_iplugin == null) {
                Console.WriteLine("Non-plugin dll file {0}", Path.GetFileName(file));
                return;
            }
            try {
                Object o = Activator.CreateInstance(t_iplugin);
                IPlugin plugin = (IPlugin)o;
                Register(plugin);
            }
            catch (Exception e) {
                Console.WriteLine("Cannot create plugin instance for {0}",file);
                Console.WriteLine(e);
            }
        }
        /// <summary>
        /// Loads all plugins in the /plugins/ directory
        /// </summary>
        public void LoadAll() {
            try {
                string[] files = Directory.GetFiles("./plugins/", "*.dll");
                if (files.Length == 0)
                    throw new Exception();
                foreach (string f in files) {
                    Load(Path.Combine(Environment.CurrentDirectory, f));
                }
                lock (_plugins) {
                    for (int i=0; i < _plugins.Count;i++) {
                        IPlugin p = _plugins.ElementAt(i);
                        try {
                            if (!p.OnAllLoaded()) {
                                ConsoleColor c = Console.ForegroundColor;
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine(@"Plugin ""{0}"" failed to finish initialization after loading was completed and will not be loaded", p.Name);
                                Console.ForegroundColor = c;
                                _plugins.RemoveAt(i);
                                i--;
                            }
                        }
                        catch (Exception e) {
                            ConsoleColor c = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Plugin \"{0}\" failed to finish initialization after loading was completed and will not be loaded, it threw the following exception: \n {1}", p.Name,e);
                            Console.ForegroundColor = c;
                            _plugins.RemoveAt(i);
                            i--;
                        }

                    }
                }
            } catch (Exception) {
                Console.WriteLine("No plugins loaded.");
            }
        }
        void Register(IPlugin plugin) {
            lock (_plugins) {
                _plugins.Add(plugin);
             }

            if (plugin.OnLoad())
                Console.WriteLine("Loaded plugin {0} {1}", plugin.Name, plugin.Version);
            else {
                Console.WriteLine("Plugin {0} {1} failed to load correctly.", plugin.Name, plugin.Version);
                lock (_plugins) {
                    _plugins.Remove(plugin);
                }
            }
        }
        /// <summary>
        /// Returns all currently loaded plugins
        /// </summary>
        /// <returns>Array of IPlugin - the interface which all plugin entry points must implement</returns>
        public IPlugin[] GetPlugins() {
            IPlugin[] list = new IPlugin[_plugins.Count];
            _plugins.CopyTo(list);
            
            return list;
        }

    }
}
