﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace St0rm.TF2.Core.Plugin {
    /// <summary>
    /// All plugins must have a class implementing this interface to be recognized
    /// </summary>
    public interface IPlugin {
        /// <summary>
        /// Name of the plugin
        /// </summary>
        string Name { get; }
        /// <summary>
        /// Version of the plugin
        /// </summary>
        string Version { get; }
        /// <summary>
        /// Author of the plugin
        /// </summary>
        string Author { get; }
        /// <summary>
        /// Called when the plugin is loaded
        /// </summary>
        /// <returns>Initialization success</returns>
        bool OnLoad();
        /// <summary>
        /// Called when all plugins have loaded
        /// </summary>
        /// <returns>Initialization success</returns>
        bool OnAllLoaded(); 
    }
}
