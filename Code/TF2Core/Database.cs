﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace St0rm.TF2.Core {
    /// <summary>
    /// Used to accessing the MySQL database
    /// </summary>
    public class Database {
        static Database __instance = new Database();
        MySqlConnection _connection = null;
        int _connection_attempts = 0;
        /// <summary>
        ///The singleton instance
        /// </summary>
        public static Database Instance { get { return __instance; } }
        /// <summary>
        /// The MySqlConnection
        /// </summary>
        public MySqlConnection Connection {
            get {
                try {
                    if (_connection != null && _connection.State == System.Data.ConnectionState.Open) {
                        return _connection;
                    }
                    else {
                        _connection = new MySqlConnection(String.Format("Server={0};Database={1};Uid={2};Pwd={3};", Config.DBHost, Config.DBName, Config.DBUser, Config.DBPass));
                        _connection.Open();
                        _connection_attempts = 0;
                        return _connection;
                    }
                }
                catch (MySqlException e) {
                    if (_connection_attempts < 5) {
                        _connection_attempts++;
                        return Connection;
                    }
                    else {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("MySQL failed to connect five (5) times, last exception: {0}",e);
                        Environment.Exit(1);
                        throw e;
                        
                    }
                }
            }
        }
    }
}
