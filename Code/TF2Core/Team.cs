﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace St0rm.TF2.Core {
    /// <summary>
    /// Represents one of the teams of TF2
    /// </summary>
    public class Team {
        /// <summary>
        /// The BLUE team
        /// </summary>
        public static readonly Team BLUE = new Team();
        /// <summary>
        /// The RED team
        /// </summary>
        public static readonly Team RED = new Team();
        /// <summary>
        /// Team players are on before they chose
        /// </summary>
        public static readonly Team LOBBY = new Team();
        /// <summary>
        /// Any non-BLUE RED or LOBBY
        /// Usually only used for players whos team was not seen by the bot
        /// </summary>
        public static readonly Team UNKNOWN = new Team();
     
        private Team() {
        }
        /// <summary>
        /// Calls the Reset method on all teams
        /// </summary>
        public static void ResetAll() {
            RED.Reset();
            BLUE.Reset();
            LOBBY.Reset();
            UNKNOWN.Reset();
        }
        /// <summary>
        /// Gets the team with the specified name
        /// </summary>
        /// <param name="name">Name of the team ie Red or Blue</param>
        /// <returns>The Team this name belongs to</returns>
        public static Team GetTeam(string name) {


            if (String.Compare(name, "Red", true) == 0)
                return Team.RED;
            else if (String.Compare(name, "Blue", true) == 0)
                return Team.BLUE;
            else if (String.Compare(name, "Lobby", true) == 0)
                return Team.LOBBY;
            else
                return Team.UNKNOWN;
        }

        int _score = 0;
        System.Threading.Mutex _m_score = new System.Threading.Mutex(false, "Team Score");

        /// <summary>
        /// Gets or Sets the score of the team
        /// </summary>
        public int Score {
            get {
                lock (_m_score) {
                    return _score;
                }
            }
            set {
                lock (_m_score) {
                    _score = value; 
                }
            }
        }

        /// <summary>
        /// Colors the given text with the specified team's color
        /// </summary>
        /// <param name="text">Text to color</param>
        /// <returns>team-colored text</returns>
        public string GetColor(string text) {
            StringBuilder sb = new StringBuilder();
            if (this == BLUE)
                sb.Append("12");
            else if (this == RED)
                 sb.Append("04");
            else
                 sb.Append("");
            sb.Append(text);
            if (this != UNKNOWN && this != LOBBY)
                sb.Append("");
            return sb.ToString();
        }
        /// <summary>
        /// Deletes all data for this team making it as it is on round start
        /// </summary>
        public void Reset() {
            _score = 0;
        }
        /// <summary>
        /// Gets all players on this team
        /// </summary>
        /// <returns>Player[] for this team</returns>
        public Player[] GetPlayers() {
            return Player.GetPlayerList(this);
        }
        /// <summary>
        /// Returns display name for the team
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            if (this == BLUE)
                return "Blue";
            else if (this == RED)
                return "Red";
            else if (this == LOBBY)
                return "Lobby";
            else if (this == UNKNOWN)
                return "Unknown";
            else
                return "UNDEFINED";
        }

    }

}
