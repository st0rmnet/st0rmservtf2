﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using St0rm.TF2.SSQWrapper;
using DPDN.Irc.Client;
using DPDN.Irc;
using St0rm.TF2.Core;

namespace St0rm.TF2.IRCCommands {
    class ICRaw {
        static ICRaw _instance = new ICRaw();
        public static ICRaw Instance { get { return _instance; } }
        private ICRaw() {
        
            IRC.Instance.OnIrcCommand += Handle;
        }
        public bool Handle(string command, string parameters, ChannelMessageReceivedEventArgs data) {
            if (command.ToLower().Equals("rcon") && (data.User.IsAdmin(data.Channel.Name) || data.User.IsOwner(data.Channel.Name))) {
                Remote.Raw(parameters);
                return true;
            }
            else if (command.ToLower().Equals("rehash") && (data.User.IsOp(data.Channel.Name) || data.User.IsAdmin(data.Channel.Name) || data.User.IsOwner(data.Channel.Name))) {
                Config.Load();
                IRC.Instance.Broadcast("Configuration and data tables re-hashed");
                return true;
            }
            else if (command.ToLower().Equals("refresh") && (data.User.IsOp(data.Channel.Name) || data.User.IsAdmin(data.Channel.Name) || data.User.IsOwner(data.Channel.Name))) {
                Player.RefreshList();
                IRC.Instance.Broadcast("Refreshed player listing");
                return true;
            }
            else if (command.ToLower().Equals("revive") && (data.User.IsOp(data.Channel.Name) || data.User.IsAdmin(data.Channel.Name) || data.User.IsOwner(data.Channel.Name))) {
                try {
                    SSQ.Instance.SetGameServer(Config.TF2Host, Config.TF2Port);
                    IRC.Client.SendM(data.Channel.Name, "New rcon challenge number received successfully - rcon should function properly.");
                }
                catch (RequestFailedException) {
                    IRC.Client.SendM(data.Channel.Name, "Request failed - check that the server is online.");
                }
            }
            else if (command.ToLower().Equals("addlog") && (data.User.IsOp(data.Channel.Name) || data.User.IsAdmin(data.Channel.Name) || data.User.IsOwner(data.Channel.Name))) {
                TF2EventHandler.Instance.LogHandler("-" + parameters);
            }
            else if (command.ToLower().Equals("ssqlock")) {
                Console.WriteLine("Locking on SSQ.Instance._mutex..."); 
                lock (SSQ.Instance._mutex) {
                    Console.WriteLine("Successful");
                }
            }
            else if (command.ToLower().Equals("playerlock")) {
                Console.WriteLine("Getting player list...");
                Player.GetPlayerList();
                    Console.WriteLine("Successful");
                
            }
            return false;
        }
    }
}
