﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
namespace St0rm.TF2.IRCCommands
{
    using System;
    using System.Text;
    using System.Text.RegularExpressions;
    using Core;
    using DPDN.Irc.Client;
    using MySql.Data.MySqlClient;

    internal class ICBotInfo
    {
        private static readonly ICBotInfo _instance = new ICBotInfo();

        private ICBotInfo()
        {
            IRC.Instance.OnIrcCommand += Handle;
        }

        public static ICBotInfo Instance
        {
            get { return _instance; }
        }

        public bool Handle(string command, string parameters, ChannelMessageReceivedEventArgs data)
        {
            if (command.ToLower().Equals("modules"))
            {
                if (parameters.ToLower().Equals("list"))
                {
                    MySqlCommand cmd = Database.Instance.Connection.CreateCommand();
                    cmd.CommandText = "SELECT * FROM modules LIMIT 100";
                    cmd.Prepare();
                    MySqlDataReader reader = cmd.ExecuteReader();
                    var sb = new StringBuilder();
                    while (reader.Read())
                    {
                        sb.Append((reader.GetBoolean(1) ? "03" : "04") + reader.GetString(0) + " ");
                    }
                    reader.Close();
                    if (sb.Length > 0)
                    {
                        IRC.Client.Send(String.Format("NOTICE {0} :{1}", data.User.Name, "Modules: " + sb));
                    }
                    else
                        IRC.Client.Send(String.Format("NOTICE {0} :{1}", data.User.Name, "There are no modules"));
                }
                else if (Regex.IsMatch(parameters, "(?i)^toggle .+"))
                {
                    if (data.User.IsOp(data.Channel.Name))
                    {
                        string item = parameters.Substring(parameters.IndexOf(" ") + 1);
                        if (Config.Toggle(item))
                            IRC.Client.Send(String.Format("NOTICE {0} :Module toggled: {1}", data.User.Name, item));
                        else
                            IRC.Client.Send(String.Format("NOTICE {0} :{1}",
                                data.User.Name,
                                "Module could not be changed"));
                    }
                    else
                        IRC.Client.Send(String.Format("NOTICE {0} :{1}",
                            data.User.Name,
                            "You must be at least an operator to toggle a module"));
                }
                else
                    IRC.Client.Send(String.Format("NOTICE {0} :{1}",
                        data.User.Name,
                        "Syntax: !modules <list|toggle> [module]"));
            }
            return false;
        }
    }
}
