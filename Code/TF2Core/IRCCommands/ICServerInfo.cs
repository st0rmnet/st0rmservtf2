﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
namespace St0rm.TF2.IRCCommands
{
    using System;
    using System.Text;
    using System.Text.RegularExpressions;
    using Core;
    using DPDN.Irc.Client;
    using SSQWrapper;

    internal class ICServerInfo
    {
        private static readonly ICServerInfo _instance = new ICServerInfo();

        private ICServerInfo()
        {
            IRC.Instance.OnIrcCommand += Handle;
        }

        public static ICServerInfo Instance
        {
            get { return _instance; }
        }

        public bool Handle(string command, string parameters, ChannelMessageReceivedEventArgs data)
        {
            if (command.ToLower().Equals("pl"))
            {
                Player[] players;

                players = Player.GetPlayerList(Team.RED);
                var sb = new StringBuilder("[04Red]:");
                sb.Append(" (" + players.Length + ")");
                foreach (var pl in players)
                {
                    sb.AppendFormat(" \"{0}\" ({1})", pl.Name, pl.Id);
                }
                if (players.Length == 0)
                {
                    sb.Append(" No players");
                }

                IRC.Client.SendM(data.Channel.Name, sb.ToString());
                
                players = Player.GetPlayerList(Team.BLUE);
                sb = new StringBuilder("[12Blue]:");
                sb.Append(" (" + players.Length + ")");
                foreach (var pl in players)
                {
                    sb.AppendFormat(" \"{0}\" ({1})", pl.Name, pl.Id);
                }
                if (players.Length == 0)
                {
                    sb.Append(" No players");
                }

                IRC.Client.SendM(data.Channel.Name, sb.ToString());

                players = Player.GetPlayerList(Team.UNKNOWN);
                if (players.Length > 0)
                {
                    sb = new StringBuilder("[Unknown]:");
                    sb.Append(" (" + players.Length + ")");
                    foreach (var pl in players)
                    {
                        sb.AppendFormat(" \"{0}\" ({1})", pl.Name, pl.Id);
//                        sb.Append(" \"" + pl.Name + "\"");
                    }
                        
                    IRC.Client.SendM(data.Channel.Name, sb.ToString());
                }

                players = Player.GetPlayerList(Team.LOBBY);
                if (players.Length > 0)
                {
                    sb = new StringBuilder("[Lobby]:");
                    sb.Append(" (" + players.Length + ")");
                    foreach (var pl in players)
                    {
                        sb.AppendFormat(" \"{0}\" ({1})", pl.Name, pl.Id);
                    }
                    IRC.Client.SendM(data.Channel.Name, sb.ToString());
                }
                return true;
            }
            else if (command.ToLower().Equals("gi"))
            {
                Info_Reply info = SSQ.Instance.GetInfoReply();
                IRC.Client.SendM(data.Channel.Name, "[10GameInfo]: Server Name: " + info.hostname);
                IRC.Client.SendM(data.Channel.Name, "[10GameInfo]: Description: " + info.game_description);
                IRC.Client.SendM(data.Channel.Name,
                    "[10GameInfo]:     Players: " + info.num_players + "/" + info.max_players + " (Actual " +
                    Player.GetPlayerList().Length + ") Bots: " + info.num_of_bots + " Map: " + info.map);
                IRC.Client.SendM(data.Channel.Name,
                    "[10GameInfo]:     Version: " + (int)info.version + " Other: " +
                    (info.os == 'l' ? "*NIX" : "Windows") + (info.secure == 1 ? " VAC-Enabled" : " Non-VAC") +
                    (info.password == 1 ? " Passworded" : " Open"));
                return true;
            }
            else if (command.ToLower().Equals("pi"))
            {
                if (parameters.Equals(""))
                    IRC.Client.SendM(data.Channel.Name,
                        "Syntax: !pi <name/id> -> Shows Player Information for the given player");
                else
                {
                    Player target;
                    if (Regex.IsMatch(parameters, "^\\d+$") && Player.GetPlayer(parameters) == null)
                    {
                        target = Player.GetPlayer(int.Parse(parameters));
                        if (target == null)
                            IRC.Client.SendM(data.Channel.Name, "No such player ID, use !pl for a list");
                    }
                    else
                    {
                        target = Player.GetPlayer(parameters);
                        if (target == null)
                        {
                            Player[] matches = Player.FindPlayers(parameters);
                            if (matches.Length == 0)
                                IRC.Client.SendM(data.Channel.Name, "No such player, use !pl for a list");
                            else if (matches.Length > 1)
                                IRC.Client.SendM(data.Channel.Name,
                                    matches.Length +
                                    " players match this name, please be more specific or use the player ID");
                            else
                                target = matches[0];
                        }
                    }
                    if (target == null)
                        return true;
                    else
                    {
                        string ipcensor = target.IP;
                        string[] parts  = target.IP.Split('.');

                        if (parts.Length > 1)
                        {
                            parts[parts.Length - 1] = "IPE";
                            ipcensor                = String.Join(".", parts);
                        }

                        //ID[2] Name[lazrspewpew] Ping[147] Score[1394] IPAddress[99.241.10.IPE] SerialHash[58cba4ca7edf25243df6280f44b1b0cb]Bandwidth[94 kB/s] Played[000:19:02] Rank[4 (8k / 4d = 2.0)] Credits[3410]
                        string reply =
                            String.Format(
                                          "[07PlayerInfo]: 10ID[{0}] Name[{1}10] SteamID[{2}] IPAddress[{3}] Played[{4}] Kills[{5}]"
                                          ,
                                target.Id,
                                target.ColoredName,
                                target.SteamID,
                                ipcensor,
                                target.StaySpan,
                                target.Kills);
                        IRC.Client.SendM(data.Channel.Name, reply);
                    }
                }
                return true;
            }
            return false;
        }
    }
}
