﻿// Copyright 2009 Benjamin "aca20031" Buzbee
// 
// This file is part of St0rmServ Team Fortres 2 IRC Bot ("St0rmServ_TF2", "St0rmServ TF2").
// 
// St0rmServ TF2 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// St0rmServ TF2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
namespace St0rm.TF2.IRCCommands
{
    using System;
    using System.Text.RegularExpressions;
    using Core;
    using DPDN.Irc;
    using DPDN.Irc.Client;

    internal class ICCommunicate
    {
        private static readonly ICCommunicate _instance = new ICCommunicate();

        private ICCommunicate()
        {
            IRC.Instance.OnIrcCommand += Handle;
        }

        public static ICCommunicate Instance
        {
            get { return _instance; }
        }

        public bool Handle(string command, string parameters, ChannelMessageReceivedEventArgs data)
        {
            if (Regex.IsMatch(command, "^(?i)msg|message|say$") && !parameters.Trim().Equals(""))
            {
                User usr = data.User;
                if (usr.IsOp(data.Channel.Name) || usr.IsHalfOp(data.Channel.Name) || usr.IsVoice(data.Channel.Name))
                    Remote.Say(String.Format("({0}@IRC): {1}", usr.Name, parameters));
                else
                    IRC.Client.Send(String.Format("NOTICE {0} :{1}",
                        data.User.Name,
                        "You must at least be voiced before you can message the server."));
                return true;
            }
            else if (Regex.IsMatch(command, "^(?i)me|hostact|act|action|describe$"))
            {
                User usr = data.User;
                if (usr.IsOp(data.Channel.Name) || usr.IsHalfOp(data.Channel.Name) || usr.IsVoice(data.Channel.Name))
                    Remote.Say("*{0} {1}", data.User.Name, parameters);
                else
                    IRC.Client.Send(String.Format("NOTICE {0} :{1}",
                        data.User.Name,
                        "You must at least be voiced before you can message the server."));
                return true;
            }
            else if (Regex.IsMatch(command, "^(?i)hmsg|cmsg$"))
            {
                if (data.User.IsOp(data.Channel.Name))
                    Remote.Say("{0}", parameters);
                else
                    IRC.Client.Send(String.Format("NOTICE {0} :{1}",
                        data.User.Name,
                        "You must at least be opped before you can message the server as the console."));
                return true;
            }
            return false;
        }
    }
}
